/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2012 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 *
 * Configuration settings for congatec conga-QMX6
 * Derived from Freescale i.MX6Q SabreSD board.
 */

#ifndef __CGTQMX6_H
#define __CGTQMX6_H

#include <linux/stringify.h>
#include "mx6_common.h"

#ifdef CONFIG_SPL
#include "imx6_spl.h"
#endif


/* Physical Memory Map */
#define PHYS_SDRAM                     MMDC0_ARB_BASE_ADDR

#define CONFIG_SYS_SDRAM_BASE          PHYS_SDRAM
#define CONFIG_SYS_INIT_RAM_ADDR       IRAM_BASE_ADDR
#define CONFIG_SYS_INIT_RAM_SIZE       IRAM_SIZE

#define CONFIG_SYS_INIT_SP_OFFSET \
	(CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
#define CONFIG_SYS_INIT_SP_ADDR \
	(CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)


#ifdef CONFIG_DISPLAY
#define CONFIG_IMX_HDMI
#define CONFIG_IMX_VIDEO_SKIP

/* Framebuffer */
#define CONFIG_VIDEO_BMP_RLE8
#define CONFIG_BMP_16BPP
#define CONFIG_VIDEO_LOGO
#define CONFIG_VIDEO_BMP_LOGO
#define CONFIG_IMX6_PWM_PER_CLK        66000000
#endif

#define CONFIG_MACH_TYPE        4122

/* Debug Console */
#define CONFIG_MXC_UART_BASE    UART2_BASE
#define CONSOLE_DEV             "ttymxc1"

/* Boot Device
 * - mmc0: QMX6 onboard micro-SD slot
 * - mmc1: QMX6 eMMC
 * - mmc2: QMX6 Q7-SD-interface
 */
#define CGT_ENV_MMCDEV           0                /* device to load kernel from */
#define CGT_ENV_MMCROOT         "/dev/mmcblk0p1"  /* kernel command line */

/* cgtqmx6 Rev.E:
 * CGT_MMC_BLK_REMOVE: Downgrade SD/MMC from UHS/HS200/HS400 modes before boot.
 *
 * Older kernel versions or systems which do not connect eMMC reset line
 * properly may not be able to handle situations where either the eMMC
 * is left in HS200/HS400 mode or SD card in UHS modes by the bootloader
 * and may misbehave.
 *
 * Attention: enabling this function leads to two warnings when switching back
 * to low-speed mode on u-boot cleanup:
 *	esdhc_change_pinstate 2 error
 *	esdhc_set_timing error -38
 * MMC_BLK_REMOVE tries to switch back to SD_HS mode very late in cleanup
 * and the pinconfigs for the usdhc node in the device tree are already
 * freed and no longer available. This causes the pinstate error which
 * leads then to the set_timing error.
 *
 * On conga-QMX6 Rev.E this option is disabled by default so the interfaces
 * keep their high speed modes when Kernel takes control. SD-card is
 * power-cycled on Kernel startup and eMMC does not use HS200/HS400 mode.
 */
// #define CGT_MMC_BLK_REMOVE

#ifdef ENV_IS_IN_MMC
#define CONFIG_SYS_MMC_ENV_DEV   CGT_ENV_MMCDEV
#define CGT_ENV_AUTODETECT       "mmcautodetect=yes\0"
#else
#define CGT_ENV_AUTODETECT       ""
#endif

/* MMC Configs */
#define CONFIG_SYS_FSL_ESDHC_ADDR      0

/* Network */
#ifdef CONFIG_MII
#define CONFIG_FEC_MXC
#endif
#ifdef CONFIG_CMD_NET
#define CONFIG_ETHPRIME                "eth0"
#endif

/* SATA */
#ifdef CONFIG_SATA
#define CONFIG_SYS_SATA_MAX_DEVICE 1
#define CONFIG_SYS_SATA_ENV_DEV 1
#endif

/* Falcon Mode */
#define CONFIG_SPL_FS_LOAD_ARGS_NAME    "args"
#define CONFIG_SPL_FS_LOAD_KERNEL_NAME  "uImage"
#define CONFIG_SYS_SPL_ARGS_ADDR        0x18000000

/* Falcon Mode - MMC support: args@1MB kernel@2MB */
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTOR    0x0800  /* 1MB */
#define CONFIG_SYS_MMCSD_RAW_MODE_ARGS_SECTORS   (CONFIG_CMD_SPL_WRITE_SIZE / 512)
#define CONFIG_SYS_MMCSD_RAW_MODE_KERNEL_SECTOR  0x1000  /* 2MB */

#define CONFIG_SYS_FSL_USDHC_NUM       3

/*
 * imx6 q/dl/solo pcie would be failed to work properly in kernel, if
 * the pcie module is iniialized/enumerated both in uboot and linux
 * kernel.
 * rootcause:imx6 q/dl/solo pcie don't have the reset mechanism.
 * it is only be RESET by the POR. So, the pcie module only be
 * initialized/enumerated once in one POR.
 * Set to use pcie in kernel defaultly, mask the pcie config here.
 * Remove the mask freely, if the uboot pcie functions, rather than
 * the kernel's, are required.
 */
#ifdef CONFIG_CMD_PCI
#define CONFIG_PCI_SCAN_SHOW
#define CONFIG_PCIE_IMX
#endif

/* USB Configs */
#ifdef CONFIG_CMD_USB
#define CONFIG_EHCI_HCD_INIT_AFTER_RESET
#define CONFIG_MXC_USB_PORTSC                    (PORT_PTS_UTMI | PORT_PTS_PTW)
#define CONFIG_MXC_USB_FLAGS                     0
#define CONFIG_USB_MAX_CONTROLLER_COUNT          1 /* Enabled USB controller number */
#endif

#define CONFIG_CMD_READ
#define CONFIG_SERIAL_TAG
#define CONFIG_FASTBOOT_USB_DEV 0
#define CONFIG_USBD_HS

/* I2C Configs */
#ifdef CONFIG_CMD_I2C
#define CONFIG_SYS_I2C_MXC
#define CONFIG_SYS_I2C_SPEED		  100000
#endif

/* PMIC */
#ifndef CONFIG_DM_PMIC
#define CONFIG_POWER
#define CONFIG_POWER_I2C
#define CONFIG_POWER_PFUZE100
#define CONFIG_POWER_PFUZE100_I2C_ADDR 0x08
#define CGTQMX6_PFUZE_I2C_MUX_GPIO     IMX_GPIO_NR(6, 9)
#define CGTQMX6_PFUZE_I2C_BUS          1
#endif

#define MFG_BOOT_CMD "bootm "

#ifdef CONFIG_USB_PORT_AUTO
    #define FASTBOOT_CMD "echo \"Run fastboot ...\"; fastboot auto; "
#else
    #define FASTBOOT_CMD "echo \"Run fastboot ...\"; fastboot 0; "
#endif

/* For Rev.C modules bootcmd_mfg has to be specified otherwise bootloader
 * boots directly into OS and no firmware update is possible
 */
#define MFG_ENV_SETTINGS_DEFAULT \
	"tee=no\0" \
	"mfgtool_args=setenv bootargs console=${console},${baudrate} earlycon no_console_suspend" \
		"${displayinfo}\0" \
	"bootcmd_mfg=run mfgtool_args; " \
		"if iminfo ${initrd_addr}; then " \
			"if test ${tee} = yes; then " \
				"bootm ${tee_addr} ${initrd_addr} ${fdt_addr}; " \
			"else " \
				MFG_BOOT_CMD "${loadaddr} ${initrd_addr} ${fdt_addr}; " \
			"fi; " \
		"else " \
			FASTBOOT_CMD  \
		"fi;\0" \


/* Environment */
#define CONFIG_MFG_ENV_SETTINGS \
	MFG_ENV_SETTINGS_DEFAULT \
	"initrd_addr=0x12C00000\0" \
	"initrd_high=0xffffffff\0" \


#ifdef CONFIG_DISPLAY
#define CGT_DISPLAY_ENV \
	"panel=Hannstar-XGA\0" \
	"backlight_level=80\0"
#else
#define CGT_DISPLAY_ENV
#endif

#ifndef CONFIG_SPL
#define CGT_FDT_FILE \
	"fdt_file=" CONFIG_DEFAULT_FDT_FILE "\0"
#else
#define CGT_FDT_FILE \
	"fdt_file=undefined\0"
#endif

#define CONFIG_EXTRA_ENV_SETTINGS \
	CONFIG_MFG_ENV_SETTINGS \
	CGT_DISPLAY_ENV \
	"script=boot.scr\0" \
	"image=uImage\0" \
	CGT_FDT_FILE \
	"fdt_addr=0x18000000\0" \
	"tee_addr=0x20000000\0" \
	"tee_file=undefined\0" \
	"boot_fdt=try\0" \
	"ip_dyn=yes\0" \
	"console=" CONSOLE_DEV "\0" \
	"dfuspi=dfu 0 sf 0:0:10000000:0\0" \
	"dfu_alt_info_spl=spl raw 0x400\0" \
	"dfu_alt_info_img=u-boot raw 0x10000\0" \
	"dfu_alt_info=spl raw 0x400\0" \
	"fdt_high=0xffffffff\0"	  \
	"initrd_high=0xffffffff\0" \
	"splashimage=0x28000000\0" \
	"mmcdev=" __stringify(CGT_ENV_MMCDEV) "\0" \
	"mmcpart=1\0" \
	"finduuid=part uuid mmc ${mmcdev}:2 uuid\0" \
	"mmcroot=" CGT_ENV_MMCROOT " rootwait rw\0" \
	CGT_ENV_AUTODETECT \
	"update_sd_firmware=" \
		"if test ${ip_dyn} = yes; then " \
			"setenv get_cmd dhcp; " \
		"else " \
			"setenv get_cmd tftp; " \
		"fi; " \
		"if mmc dev ${mmcdev}; then "	\
			"if ${get_cmd} ${update_sd_firmware_filename}; then " \
				"setexpr fw_sz ${filesize} / 0x200; " \
				"setexpr fw_sz ${fw_sz} + 1; "	\
				"mmc write ${loadaddr} 0x2 ${fw_sz}; " \
			"fi; "	\
		"fi\0" \
	"mmcargs=setenv bootargs console=${console},${baudrate} ${smp} earlycon no_console_suspend " \
		"root=${mmcroot} ${displayinfo}\0" \
	"loadbootscript=" \
		"load mmc ${mmcdev}:${mmcpart} ${loadaddr} boot/${script};\0" \
	"bootscript=echo Running bootscript from mmc ...; " \
		"source\0" \
	"loadimage=echo Loading ${image}; load mmc ${mmcdev}:${mmcpart} ${loadaddr} boot/${image}\0" \
	"loadfdt=echo Loading ${fdt_file}; load mmc ${mmcdev}:${mmcpart} ${fdt_addr} boot/${fdt_file}\0" \
	"loadtee=echo Loading ${tee_file}; load mmc ${mmcdev}:${mmcpart} ${tee_addr} boot/${tee_file}\0" \
	"mmcboot=echo Booting from mmc ...; " \
		"run mmcargs; " \
		"if test ${tee} = yes; then " \
			"run loadfdt; run loadtee; bootm ${tee_addr} - ${fdt_addr}; " \
		"else " \
			"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
				"if run loadfdt; then " \
					"bootm ${loadaddr} - ${fdt_addr}; " \
				"else " \
					"if test ${boot_fdt} = try; then " \
						"bootm; " \
					"else " \
						"echo WARN: Cannot load the DT; " \
					"fi; " \
				"fi; " \
			"else " \
				"bootm; " \
			"fi;" \
		"fi;\0" \
	"netargs=setenv bootargs console=${console},${baudrate} ${smp} no_console_suspend earlycon " \
		"root=/dev/nfs " \
		"ip=dhcp nfsroot=${serverip}:${nfsroot},v3,tcp ${displayinfo}\0" \
	"netboot=echo Booting from net ...; " \
		"run findfdt; " \
		"run netargs; " \
		"if test ${ip_dyn} = yes; then " \
			"setenv get_cmd dhcp; " \
		"else " \
			"setenv get_cmd tftp; " \
		"fi; " \
		"${get_cmd} ${image}; " \
		"if test ${tee} = yes; then " \
			"${get_cmd} ${tee_addr} ${tee_file}; " \
			"${get_cmd} ${fdt_addr} ${fdt_file}; " \
			"bootm ${tee_addr} - ${fdt_addr}; " \
		"else " \
			"if test ${boot_fdt} = yes || test ${boot_fdt} = try; then " \
				"if ${get_cmd} ${fdt_addr} ${fdt_file}; then " \
					"bootm ${loadaddr} - ${fdt_addr}; " \
				"else " \
					"if test ${boot_fdt} = try; then " \
						"bootm; " \
					"else " \
						"echo WARN: Cannot load the DT; " \
					"fi; " \
				"fi; " \
			"else " \
				"bootm; " \
			"fi; " \
		"fi;\0" \
	"findfdt="\
		"if test $fdt_file = undefined; then " \
			"if test $board_name = CGTQMX6 && test $board_rev = MX6DQ; then " \
				"setenv fdt_file imx6q-cgtqmx6; " \
			"else if test $board_name = CGTQMX6 && test $board_rev = MX6SDL; then " \
				"setenv fdt_file imx6dl-cgtqmx6; fi; " \
			"fi; " \
			/* check for larger than C so a D,E,F,... fits */ \
			"if test $board_hw_rev > C; then " \
				"setenv fdt_file ${fdt_file}-revE; " \
			"fi; " \
			"if test $fdt_file = undefined; then " \
				"echo WARNING: Could not determine dtb to use; " \
			"else " \
				"setenv fdt_file ${fdt_file}.dtb;" \
			"fi; " \
		"fi;\0" \
	"findtee="\
		"if test $tee_file = undefined; then " \
			"if test $board_name = CGTQMX6 && test $board_rev = MX6DQ; then " \
				"setenv tee_file uTee-6qsdb; " \
			"else if test $board_name = CGTQMX6 && test $board_rev = MX6SDL; then " \
				"setenv tee_file uTee-6dlsdb; fi; " \
			"fi; " \
			"if test $tee_file = undefined; then " \
				"echo WARNING: Could not determine tee to use; fi; " \
		"fi;\0" \


#endif /* __CGTQMX6_H */
