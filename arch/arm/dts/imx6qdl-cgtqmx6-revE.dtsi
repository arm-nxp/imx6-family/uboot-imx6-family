// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#include "imx6qdl-cgtqmx6.dtsi"

#include <dt-bindings/net/ti-dp83867.h>
#include "../../../board/congatec/common/eth/ti_dp83867.h"

/ {
	regulators {
		/* onboard microSD:
	 	   reset line: MX6QDL_PAD_DISP0_DAT12__GPIO5_IO06, nc/np on Rev.E
	 	   enable line: MX6QDL_PAD_DISP0_DAT13__GPIO5_IO07, external pullup
		   DISP0_DAT12 and _DAT13 not used on Rev. A..C */
		reg_usdhc2_pwr: reg_usdhc2_pwr {
			compatible = "regulator-fixed";
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			regulator-name = "sd_power_en";
			enable-active-high;
			startup-delay-us = <100>;
			gpio = <&gpio5 7 GPIO_ACTIVE_HIGH>;
		};


		/* onboard microSD card voltage selector
		 * MX6QDL_PAD_DISP0_DAT11__GPIO5_IO05
		 * DISP0_DAT11 not used on Rev. A..C */
		reg_usdhc2_vselect: reg_usdhc2_vselect {
			compatible = "regulator-gpio";
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <3300000>;
			regulator-name = "sd_sel_1v8";
			enable-active-high;
			gpios = <&gpio5 5 GPIO_ACTIVE_HIGH>;
			startup-delay-us = <100>;
			states = <1800000 0x1
			          3300000 0x0>;
			vin-supply = <&reg_3p3v>;
		};

	};
};

/* Onboard u-SD slot */
&usdhc2 {
	/delete-property/ no-1-8-v;
	/delete-property/ keep-power-in-suspend;
	/delete-property/ enable-sdio-wakeup;
	/delete-property/ vmmc-supply;

	pinctrl-names = "default", "state_100mhz", "state_200mhz";
	pinctrl-0 = <&pinctrl_usdhc2>;
	pinctrl-1 = <&pinctrl_usdhc2_100mhz>;
	pinctrl-2 = <&pinctrl_usdhc2_200mhz>;

	vmmc-supply = <&reg_usdhc2_pwr>;
	vqmmc-supply = <&reg_usdhc2_vselect>;

	// sd-uhs-sdr104;
	sd-uhs-ddr50;
	sd-uhs-sdr25;
	sd-uhs-sdr50;

};


&iomuxc {
	pinctrl_usdhc2: usdhc2grp {
		fsl,pins = <
			MX6QDL_PAD_GPIO_4__GPIO1_IO04      0x1b0b0 /* SD2_CD */
			MX6QDL_PAD_SD2_CMD__SD2_CMD        0x17069
			MX6QDL_PAD_SD2_CLK__SD2_CLK        0x10069
			MX6QDL_PAD_SD2_DAT0__SD2_DATA0     0x17069
			MX6QDL_PAD_SD2_DAT1__SD2_DATA1     0x17069
			MX6QDL_PAD_SD2_DAT2__SD2_DATA2     0x17069
			MX6QDL_PAD_SD2_DAT3__SD2_DATA3     0x17069
		>;
	};

	pinctrl_usdhc2_100mhz: usdhc2-100mhzgrp {
		fsl,pins = <
			MX6QDL_PAD_GPIO_4__GPIO1_IO04      0x1b0b0 /* SD2_CD */
			MX6QDL_PAD_SD2_CMD__SD2_CMD        0x170b9
			MX6QDL_PAD_SD2_CLK__SD2_CLK        0x100b9
			MX6QDL_PAD_SD2_DAT0__SD2_DATA0     0x170b9
			MX6QDL_PAD_SD2_DAT1__SD2_DATA1     0x170b9
			MX6QDL_PAD_SD2_DAT2__SD2_DATA2     0x170b9
			MX6QDL_PAD_SD2_DAT3__SD2_DATA3     0x170b9
		>;
	};

	pinctrl_usdhc2_200mhz: usdhc2-200mhzgrp {
		fsl,pins = <
			MX6QDL_PAD_GPIO_4__GPIO1_IO04      0x1b0b0 /* SD2_CD */
			MX6QDL_PAD_SD2_CMD__SD2_CMD        0x170f9
			MX6QDL_PAD_SD2_CLK__SD2_CLK        0x100f9
			MX6QDL_PAD_SD2_DAT0__SD2_DATA0     0x170f9
			MX6QDL_PAD_SD2_DAT1__SD2_DATA1     0x170f9
			MX6QDL_PAD_SD2_DAT2__SD2_DATA2     0x170f9
			MX6QDL_PAD_SD2_DAT3__SD2_DATA3     0x170f9
		>;
	};
};


&fec {
	mdio {
		phy: ethernet-phy@6 {

			/* delete AR8031/35 properties */
			/delete-property/ qca,clk-out-frequency;

			/* TI-DP83867 */
			ti,rx-internal-delay = <DP83867_RGMIIDCTL_2_00_NS>;
			ti,tx-internal-delay = <DP83867_RGMIIDCTL_2_00_NS>;
			ti,fifo-depth = <DP83867_PHYCR_FIFO_DEPTH_8_B_NIB>;
			ti,clk-output-sel = <DP83867_CLK_O_SEL_CHN_A_TCLK>;

			/* congatec Q7 LED specification */
			cgt,dp83867,led_0_sel = <DP83867_LED_LINK_EST>;
			cgt,dp83867,led_1_sel = <DP83867_LED_1000BT_LINK_EST>;
			cgt,dp83867,led_2_sel = <DP83867_LED_LINK_EST_TX_OR_RX_ACT>;

			/* gpio 1 as LED 3 */
			cgt,dp83867,gpio1_sel = <DP83867_GPIO_LED3>;
			cgt,dp83867,led_3_sel = <DP83867_LED_100_BTX_LINK_EST>;
		};
	};
};

&iomuxc {
	pinctrl_enet: enetgrp {
		fsl,pins = <
			/* Control */
			MX6QDL_PAD_ENET_MDIO__ENET_MDIO       0x00020 /* external pullup */
			MX6QDL_PAD_ENET_MDC__ENET_MDC         0x00020
			MX6QDL_PAD_ENET_REF_CLK__ENET_TX_CLK  0x1b0b0 /* input from phy */
			MX6QDL_PAD_ENET_TX_EN__GPIO1_IO28     0x000b0 /* RGMII INT/PWDN */
			MX6QDL_PAD_EIM_D23__GPIO3_IO23        0x00030 /* RGMII Phy Reset (ext. Pullup), DSE=25Ohm@3v3 */

			/* RGMI Interface */
			MX6QDL_PAD_RGMII_TD0__RGMII_TD0       0x00030
			MX6QDL_PAD_RGMII_TD1__RGMII_TD1       0x00030
			MX6QDL_PAD_RGMII_TD2__RGMII_TD2       0x00030
			MX6QDL_PAD_RGMII_TD3__RGMII_TD3       0x00030
			MX6QDL_PAD_RGMII_TXC__RGMII_TXC       0x00030
			MX6QDL_PAD_RGMII_TX_CTL__RGMII_TX_CTL 0x00030
			MX6QDL_PAD_RGMII_RD0__RGMII_RD0       0x10020
			MX6QDL_PAD_RGMII_RD1__RGMII_RD1       0x10020
			MX6QDL_PAD_RGMII_RD2__RGMII_RD2       0x10020
			MX6QDL_PAD_RGMII_RD3__RGMII_RD3       0x10020
			MX6QDL_PAD_RGMII_RXC__RGMII_RXC       0x10020
			MX6QDL_PAD_RGMII_RX_CTL__RGMII_RX_CTL 0x10020
		>;
	};

};
