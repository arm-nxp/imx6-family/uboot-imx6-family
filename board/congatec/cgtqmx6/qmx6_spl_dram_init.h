/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#ifndef __QMX6_SPL_DRAM_INIT_H
#define __QMX6_SPL_DRAM_INIT_H

/* initialize qmx6 dram */
void qmx6_spl_dram_init(int memsz);

#endif // __QMX6_SPL_DRAM_INIT_H