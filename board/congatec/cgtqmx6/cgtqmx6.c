// SPDX-License-Identifier: GPL-2.0+
/*
 * Board file for congatec conga-QMX6
 *
 * Copyright (C) 2012-2016 Freescale Semiconductor, Inc.
 * Copyright 2017-2018 NXP
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#include <asm/mach-imx/iomux-v3.h>
#include <asm/mach-imx/boot_mode.h>
#include <asm/arch/sys_proto.h>
#include <power/regulator.h>
#include <mmc.h>
#include <hang.h>
#include <spi.h>
#include <spi_flash.h>
#include <dm/device-internal.h>
#include <env_internal.h>
#include <fsl_wdog.h>

#if defined(CONFIG_VIDEO_IPUV3)
#include <asm/arch/clock.h>
#endif

#include "../common/eth/phy_setup.h"
#include "qmx6_mfg.h"
#include "qmx6_video.h"
#include "qmx6_pmic.h"

DECLARE_GLOBAL_DATA_PTR;

/* manufactoring data with pn and hardware revision */
static struct cgtqmx6_mfg_data mfg_data = {0};

/* global pointer to spi flash */
struct spi_flash *flash = NULL;

/* Android Buttons X5-2 */
#define KEY_VOL_UP	IMX_GPIO_NR(7, 13)


int dram_init(void)
{
	unsigned long sz = imx_ddr_size();

	/* There is a bug in imx_ddr_size for 4GiB modules:
	 * The ram size depends on detected bits and for
	 * a 4GiB module detected_bits=33.
	 * Afterwards, the ram size is calculated by (1<<bits)
	 * This is 0 for a 32 bit result value
	 *
	 * So we assume, that if zero RAM is detected, it should be 4GiB
	 */
	if (sz==0)
		/* The MX6 can do only 3840 MiB of DRAM */
		sz = 0xf0000000;

	gd->ram_size = sz;
	return 0;
}

#if CONFIG_IS_ENABLED(DM_ETH_PHY)
int board_phy_config(struct phy_device *phydev)
{
	int ret = -ENODEV;
	unsigned short id1, id2;
	const char * phy = NULL;

	/* congatec eth phy setup */
	id1 = phy_read(phydev, MDIO_DEVAD_NONE, 2);
	id2 = phy_read(phydev, MDIO_DEVAD_NONE, 3);
	debug("PHY id1=%x, id2=%x\n",id1,id2);

	/* Atheros phy on rev. A .. C */
	if ((id1 == 0x004d) && (id2 == 0xd072)) {
		ret = cgt_eth_phy_setup_ar8035(phydev);
		phy = "AR8035";
	}
	if ((id1 == 0x004d) && (id2 == 0xd074)) {
		ret = cgt_eth_phy_setup_ar8031(phydev);
		phy = "AR8031";
	}

	/* TI phy on rev. D and E */
	/* ignore revision number of phy (last nibble of id2) */
	if ((id1 == 0x2000) && ((id2 & 0xFFF0) == 0xA230)) {
		ret = cgt_eth_phy_setup_dp83867(phydev);
		phy = "DP83867";
	}

	if (ret == -ENODEV) {
		printf("Warning: Ethernet PHY configuration returns -ENODEV. "
			"Maybe missing CONFIG_PHY_<VENDOR> in defconfig?\n");
		return ret;
	}
	else if (ret) {
		printf("Warning: Ethernet PHY configuration returns non-zero value: %d\n", ret);
		return ret;
	}

	printf("%s ", phy);

	/* uboot eth phy setup */
	if (phydev->drv->config)
		phydev->drv->config(phydev);

	return 0;
}
#endif

/*
 * Do not overwrite the console
 * Use always serial for U-Boot console
 */
int overwrite_console(void)
{
	return 1;
}


/* USB controller init
   used for fastboot (UUU tool)
   and USB subsystem
*/
#ifdef CONFIG_USB_EHCI_MX6
int board_ehci_hcd_init(int port)
{
	switch (port) {
	case 0:
		/*
		  * Set daisy chain for otg_pin_id on 6q.
		 *  For 6dl, this bit is reserved.
		 */
		imx_iomux_set_gpr_register(1, 13, 1, 0);
		break;
	case 1:
		break;
	default:
		printf("MXC USB port %d not yet supported\n", port);
		return -EINVAL;
	}
	return 0;
}

#endif


int board_early_init_f(void)
{
#if defined(CONFIG_VIDEO_IPUV3)
	/* Set LDB clock to Video-PLL (PLL5) */
	select_ldb_di_clock_source(MXC_PLL5_CLK);
#endif
	return 0;
}


/* initialize spi interface and spi flash (DM interface) */
static int setup_spi_flash(void)
{
	int ret;
	struct udevice *bus_dev;
	struct udevice *flash_dev;
	unsigned int bus = CONFIG_SF_DEFAULT_BUS;
	unsigned int cs = CONFIG_SF_DEFAULT_CS;

	/* In DM mode, speed and mode will be taken from DT */
	/* this are only default values, if flash is not specified there*/
	unsigned int speed = CONFIG_SF_DEFAULT_SPEED;
	unsigned int mode = CONFIG_SF_DEFAULT_MODE;

	/* Remove the old device, otherwise probe will just be a nop */
	ret = spi_find_bus_and_cs(bus, cs, &bus_dev, &flash_dev);
	if (!ret) {
		device_remove(flash_dev, DM_REMOVE_NORMAL);
	}

	/* probe device */
	ret = spi_flash_probe_bus_cs(bus, cs, speed, mode, &flash_dev);
	if (ret) {
		printf("ERROR: Failed to initialize SPI flash at %u:%u (error %d)\n",
		       bus, cs, ret);
		return ret;
	}

	/* init spi flash */
	flash = dev_get_uclass_priv(flash_dev);
	if (!flash) {
		printf("ERROR: SPI flash not initialized");
		return -ENODEV;
	}

	return 0;
}


int board_init(void)
{
	/* address of boot parameters */
	gd->bd->bi_boot_params = PHYS_SDRAM + 0x100;

#if defined(CONFIG_DM_REGULATOR)
	regulators_enable_boot_on(false);
#endif

#ifdef CONFIG_DISPLAY
	qmx6_display_setup();
#endif

	if (setup_spi_flash())
		hang();

	/* read manufacturing data */
	if (qmx6_mfg_read_data_from_flash(flash, &mfg_data))
		hang();

#ifndef CONFIG_SPL
	/* print board info on non-SPL bootloader only
	 * on SPL-Bootloader this is done in SPL  */
	if (mfg_data.is_valid)
	{
		printf("conga-QMX6: PN:%s / Rev:%c.%c / ",
			mfg_data.pn,
			mfg_data.hwrev_major, mfg_data.hwrev_minor);
	}
	else
	{
		puts("conga-QMX6 using defaults: ");
	}

	if (is_cpu_type(MXC_CPU_MX6Q))
		puts("QuadCore\n");
	else if (is_cpu_type(MXC_CPU_MX6D))
		puts("DualCore\n");
	else if (is_cpu_type(MXC_CPU_MX6DL))
		puts("DualLite\n");
	else if (is_cpu_type(MXC_CPU_MX6SOLO))
		puts("SoloCore\n");
#endif

	return 0;
}

int power_init_board(void)
{
	return qmx6_pmic_init();
}

/* this is called from arch_preboot.os() function in cpu.c
 * when booting into os.
 */
#ifdef CONFIG_LDO_BYPASS_CHECK
void ldo_mode_set(int ldo_bypass)
{
	qmx6_ldo_set_bypass();
}
#endif


#ifdef CONFIG_CMD_BMODE
static const struct boot_mode board_boot_modes[] = {
	/* mmc0 = usdhc2: QMX6 onboard u-sd slot
	   mmc1 = usdhc3: QMX6 eMMC
	   mmc2 = usdhc4: QMX6 Q7-SD-interface */
	/* 4 bit bus width */
	{"mmc0",	MAKE_CFGVAL(0x50, 0x20, 0x00, 0x00)},
	{"mmc1",	MAKE_CFGVAL(0x50, 0x38, 0x00, 0x00)},
	{NULL,		0},
};
#endif


int board_late_init(void)
{

#ifdef CONFIG_CMD_BMODE
	add_board_boot_modes(board_boot_modes);
#endif

	env_set("tee", "no");

#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG

	env_set("board_name", "CGTQMX6");

	if (is_mx6dq()) {
		env_set("board_rev", "MX6DQ");
	}
	else if (is_mx6sdl()) {
		env_set("board_rev", "MX6SDL");

		if (is_mx6solo()) {
			env_set("smp", "nosmp");
		}
	}

	char hw_rev[2] = {0};
	hw_rev[0] = mfg_data.hwrev_major;
	env_set("board_hw_rev", hw_rev);

#endif

#ifdef CONFIG_ENV_IS_IN_MMC
	board_late_mmc_env_init();
#endif

	/* if we are booting via uuu tool from USB then do not wait */
	if (is_boot_from_usb()) {
		env_set("bootdelay", "0");
	}

#ifdef CONFIG_CMD_USB_SDP
	if (is_boot_from_usb()) {
		printf("Serial Downloader recovery mode, using sdp command\n");
		env_set("bootdelay", "0");
		env_set("bootcmd", "sdp 0");
	}
#endif

	return 0;
}

/* Always use SPI-flash first for environment.
 * Return ENVL_UNKNOWN on all other locations
 * because env-driver looks up at all configured
 * locations if no environment found on first device
 */
enum env_location env_get_location(enum env_operation op, int prio)
{
	switch (prio)
	{
	case 0: return ENVL_SPI_FLASH;
#ifdef CONFIG_ENV_IS_IN_MMC
	case 1: return ENVL_MMC;
#endif
	default:
		return ENVL_UNKNOWN;
	}
}

int checkboard(void)
{
	enum boot_device bt_dev;


	bt_dev = get_boot_device();
	puts("Boot:  ");
	switch (bt_dev) {

	/* default boot selected by fuses */
	case SPI_NOR_BOOT:
		puts("SPI Flash\n");
		break;

	/* failsafe boot  */
	// mmc0 = usdhc2 = onboard u-sd slot
	// mmc1 = usdhc3 = eMMC
	// mmc2 = usdhc4 = Q7-SD-interface
	case MMC1_BOOT:
		puts("MMC1\n");
		break;
	case MMC2_BOOT:
		puts("MMC2\n");
		break;
	case MMC3_BOOT:
		puts("MMC3\n");
		break;

	/* boot with uuu for firmware update */
	case USB_BOOT:
		puts("USB\n");
		break;

	default:
		printf("Unknown / Unsupported device %u\n", bt_dev);
		break;
	}

	return 0;
}


/* Reset the board */
void reset_cpu(void)
{
	struct watchdog_regs *wdog = (struct watchdog_regs *)WDOG1_BASE_ADDR;
	u16 wcr = WCR_WDE; // enable watchdog flag
	wcr |= WCR_WDA;    // do not assert WDOG_B line (default)
	// WCR_SRS = 0 => assert system reset signal
	// WCR_WT  = 0 => timeout = 0.5 seconds

	/* Write 3 times to ensure it works, due to IMX6Q errata ERR004346 */
	writew(wcr, &wdog->wcr);
	writew(wcr, &wdog->wcr);
	writew(wcr, &wdog->wcr);

	while (1)
		;
	/* not reached */
}


#ifdef CONFIG_MULTI_DTB_FIT
/* Identify DeviceTree name for current board */
int board_fit_config_name_match(const char *name)
{
	static const char * qmx6q = "imx6q-cgtqmx6";
	static const char * qmx6dl = "imx6dl-cgtqmx6";
	static const char * qmx6q_revE = "imx6q-cgtqmx6-revE";
	static const char * qmx6dl_revE = "imx6dl-cgtqmx6-revE";
	const char * model = NULL;

	if (!mfg_data.is_valid) {
		if (is_mx6dq())
			model = qmx6q_revE;
		else if (is_mx6sdl())
			model = qmx6dl_revE;
	}
	else {
		/* check for hardware revision of standard module */
		if (mfg_data.hwrev_major >= 'D') {
			if (is_mx6dq())
				model = qmx6q_revE;
			else if (is_mx6sdl())
				model = qmx6dl_revE;
		}
		else {
			if (is_mx6dq())
				model = qmx6q;
			else if (is_mx6sdl())
				model = qmx6dl;
		}
	}

	if (model) {
		if (!strcmp(name, model)) {
			printf("DTB: %s.dtb\n",model);
			return 0;
		}
	}

	return -1;
}
#endif



#ifdef CONFIG_OF_BOARD_FIXUP
/*
 * Adjust uboot device tree here if needed
 * @see doc/driver-model/fdt-fixup.rst
 * rw_fdt_blob: the uboot device tree blob
 */
int board_fix_fdt(void *rw_fdt_blob)
{
	int ret = 0;
	return ret;
}
#endif


#ifdef CONFIG_OF_BOARD_SETUP

/*
 * Adjust kernel dtb here if needed
 * blob: The kernel DTB
 * gd->dm_root: the u-boot device tree
 */
int ft_board_setup(void *blob, struct bd_info *bd)
{
	int ret = 0;
	return ret;
}
#endif
