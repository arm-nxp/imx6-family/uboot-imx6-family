// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#include <asm/arch/imx-regs.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/mx6-ddr.h>

const struct mx6dq_iomux_ddr_regs mx6q_ddr_ioregs = {
	.dram_sdclk_0 =  0x00000030,
	.dram_sdclk_1 =  0x00000030,
	.dram_cas =  0x00000030,
	.dram_ras =  0x00000030,
	.dram_reset =  0x00000030,
	.dram_sdcke0 =  0x00003000,
	.dram_sdcke1 =  0x00003000,
	.dram_sdba2 =  0x00000000,
	.dram_sdodt0 =  0x00000030,
	.dram_sdodt1 =  0x00000030,
	.dram_sdqs0 =  0x00000030,
	.dram_sdqs1 =  0x00000030,
	.dram_sdqs2 =  0x00000030,
	.dram_sdqs3 =  0x00000030,
	.dram_sdqs4 =  0x00000030,
	.dram_sdqs5 =  0x00000030,
	.dram_sdqs6 =  0x00000030,
	.dram_sdqs7 =  0x00000030,
	.dram_dqm0 =  0x00000030,
	.dram_dqm1 =  0x00000030,
	.dram_dqm2 =  0x00000030,
	.dram_dqm3 =  0x00000030,
	.dram_dqm4 =  0x00000030,
	.dram_dqm5 =  0x00000030,
	.dram_dqm6 =  0x00000030,
	.dram_dqm7 =  0x00000030,
};

static const struct mx6sdl_iomux_ddr_regs mx6dl_ddr_ioregs = {
	.dram_sdclk_0 = 0x00000030,
	.dram_sdclk_1 = 0x00000030,
	.dram_cas =	0x00000030,
	.dram_ras =	0x00000030,
	.dram_reset =	0x00000030,
	.dram_sdcke0 =	0x00003000,
	.dram_sdcke1 =	0x00003000,
	.dram_sdba2 =	0x00000000,
	.dram_sdodt0 =	0x00000030,
	.dram_sdodt1 =	0x00000030,
	.dram_sdqs0 =	0x00000030,
	.dram_sdqs1 =	0x00000030,
	.dram_sdqs2 =	0x00000030,
	.dram_sdqs3 =	0x00000030,
	.dram_sdqs4 =	0x00000030,
	.dram_sdqs5 =	0x00000030,
	.dram_sdqs6 =	0x00000030,
	.dram_sdqs7 =	0x00000030,
	.dram_dqm0 =	0x00000030,
	.dram_dqm1 =	0x00000030,
	.dram_dqm2 =	0x00000030,
	.dram_dqm3 =	0x00000030,
	.dram_dqm4 =	0x00000030,
	.dram_dqm5 =	0x00000030,
	.dram_dqm6 =	0x00000030,
	.dram_dqm7 =	0x00000030,
};

const struct mx6dq_iomux_grp_regs mx6q_grp_ioregs = {
	.grp_ddr_type =  0x000C0000,
	.grp_ddrmode_ctl =  0x00020000,
	.grp_ddrpke =  0x00000000,
	.grp_addds =  0x00000030,
	.grp_ctlds =  0x00000030,
	.grp_ddrmode =  0x00020000,
	.grp_b0ds =  0x00000030,
	.grp_b1ds =  0x00000030,
	.grp_b2ds =  0x00000030,
	.grp_b3ds =  0x00000030,
	.grp_b4ds =  0x00000030,
	.grp_b5ds =  0x00000030,
	.grp_b6ds =  0x00000030,
	.grp_b7ds =  0x00000030,
};

static const struct mx6sdl_iomux_grp_regs mx6sdl_grp_ioregs = {
	.grp_ddr_type = 0x000c0000,
	.grp_ddrmode_ctl = 0x00020000,
	.grp_ddrpke = 0x00000000,
	.grp_addds = 0x00000030,
	.grp_ctlds = 0x00000030,
	.grp_ddrmode = 0x00020000,
	.grp_b0ds = 0x00000030,
	.grp_b1ds = 0x00000030,
	.grp_b2ds = 0x00000030,
	.grp_b3ds = 0x00000030,
	.grp_b4ds = 0x00000030,
	.grp_b5ds = 0x00000030,
	.grp_b6ds = 0x00000030,
	.grp_b7ds = 0x00000030,
};

const struct mx6_mmdc_calibration mx6q_mmcd_calib = {
	.p0_mpwldectrl0 =  0x0016001A,
	.p0_mpwldectrl1 =  0x0023001C,
	.p1_mpwldectrl0 =  0x0028003A,
	.p1_mpwldectrl1 =  0x001F002C,
	.p0_mpdgctrl0 =  0x43440354,
	.p0_mpdgctrl1 =  0x033C033C,
	.p1_mpdgctrl0 =  0x43300368,
	.p1_mpdgctrl1 =  0x03500330,
	.p0_mprddlctl =  0x3228242E,
	.p1_mprddlctl =  0x2C2C2636,
	.p0_mpwrdlctl =  0x36323A38,
	.p1_mpwrdlctl =  0x42324440,
};

const struct mx6_mmdc_calibration mx6q_2g_mmcd_calib = {
	.p0_mpwldectrl0 =  0x00080016,
	.p0_mpwldectrl1 =  0x001D0016,
	.p1_mpwldectrl0 =  0x0018002C,
	.p1_mpwldectrl1 =  0x000D001D,
	.p0_mpdgctrl0 =    0x43200334,
	.p0_mpdgctrl1 =    0x0320031C,
	.p1_mpdgctrl0 =    0x0344034C,
	.p1_mpdgctrl1 =    0x03380314,
	.p0_mprddlctl =    0x3E36383A,
	.p1_mprddlctl =    0x38363240,
	.p0_mpwrdlctl =	   0x36364238,
	.p1_mpwrdlctl =    0x4230423E,
};

const struct mx6_mmdc_calibration mx6q_4g_mmcd_calib = {
	.p0_mpwldectrl0 =  0x00180018,
	.p0_mpwldectrl1 =  0x00220018,
	.p1_mpwldectrl0 =  0x00330046,
	.p1_mpwldectrl1 =  0x002B003D,
	.p0_mpdgctrl0 =    0x4344034C,
	.p0_mpdgctrl1 =    0x033C033C,
	.p1_mpdgctrl0 =    0x03700374,
	.p1_mpdgctrl1 =    0x03600338,
	.p0_mprddlctl =    0x443E3E40,
	.p1_mprddlctl =    0x423E3E48,
	.p0_mpwrdlctl =	   0x3C3C4442,
	.p1_mpwrdlctl =    0x46384C46,
};

static const struct mx6_mmdc_calibration mx6s_mmcd_calib = {
	.p0_mpwldectrl0 =  0x00480049,
	.p0_mpwldectrl1 =  0x00410044,
	.p0_mpdgctrl0 =    0x42480248,
	.p0_mpdgctrl1 =    0x023C023C,
	.p0_mprddlctl =    0x40424644,
	.p0_mpwrdlctl =    0x34323034,
};

static const struct mx6_mmdc_calibration mx6s_2g_mmcd_calib = {
	.p0_mpwldectrl0 =  0x00450048,
	.p0_mpwldectrl1 =  0x003B003F,
	.p0_mpdgctrl0 =    0x424C0248,
	.p0_mpdgctrl1 =    0x0234023C,
	.p0_mprddlctl =    0x40444848,
	.p0_mpwrdlctl =    0x38363232,
};

const struct mx6_mmdc_calibration mx6dl_mmcd_calib = {
	.p0_mpwldectrl0 =  0x0043004B,
	.p0_mpwldectrl1 =  0x003A003E,
	.p1_mpwldectrl0 =  0x0047004F,
	.p1_mpwldectrl1 =  0x004E0061,
	.p0_mpdgctrl0 =    0x42500250,
	.p0_mpdgctrl1 =	   0x0238023C,
	.p1_mpdgctrl0 =    0x42640264,
	.p1_mpdgctrl1 =    0x02500258,
	.p0_mprddlctl =    0x40424846,
	.p1_mprddlctl =    0x46484842,
	.p0_mpwrdlctl =    0x38382C30,
	.p1_mpwrdlctl =    0x34343430,
};

const struct mx6_mmdc_calibration mx6dl_2g_mmcd_calib = {
	.p0_mpwldectrl0 =  0x00450045,
	.p0_mpwldectrl1 =  0x00390043,
	.p1_mpwldectrl0 =  0x0049004D,
	.p1_mpwldectrl1 =  0x004E0061,
	.p0_mpdgctrl0 =    0x4240023C,
	.p0_mpdgctrl1 =	   0x0228022C,
	.p1_mpdgctrl0 =    0x02400244,
	.p1_mpdgctrl1 =    0x02340238,
	.p0_mprddlctl =    0x42464648,
	.p1_mprddlctl =    0x4446463C,
	.p0_mpwrdlctl =    0x3C38323A,
	.p1_mpwrdlctl =    0x34323430,
};

static struct mx6_ddr3_cfg mem_ddr_2g = {
	.mem_speed = 1600,
	.density = 2,
	.width = 16,
	.banks = 8,
	.rowaddr = 14,
	.coladdr = 10,
	.pagesz = 2,
	.trcd = 1310,
	.trcmin = 4875,
	.trasmin = 3500,
};

static struct mx6_ddr3_cfg mem_ddr_4g = {
	.mem_speed = 1600,
	.density = 4,
	.width = 16,
	.banks = 8,
	.rowaddr = 15,
	.coladdr = 10,
	.pagesz = 2,
	.trcd = 1310,
	.trcmin = 4875,
	.trasmin = 3500,
};

static struct mx6_ddr3_cfg mem_ddr_8g = {
	.mem_speed = 1600,
	.density = 8,
	.width = 16,
	.banks = 8,
	.rowaddr = 16,
	.coladdr = 10,
	.pagesz = 2,
	.trcd = 1310,
	.trcmin = 4875,
	.trasmin = 3500,
};


void qmx6_spl_dram_init(int memsz)
{
	/* all solo core conga-QMX6 variants has
	  a 32bit memory interface */
	int width = is_cpu_type(MXC_CPU_MX6SOLO) ? 32 : 64;
	printf("-%d / ", width);

	struct mx6_ddr_sysinfo sysinfo = {
		/* width of data bus:0=16,1=32,2=64 */
		.dsize = width / 32,
		/* config for full 4GB range so that get_mem_size() works */
		.cs_density = 32, /* 32Gb per CS */
		/* single chip select */
		.ncs = 1,
		.cs1_mirror = 0,
		.rtt_wr = 2,
		.rtt_nom = 2,
		.walat = 0,
		.ralat = 5,
		.mif3_mode = 3,
		.bi_on = 1,
		.sde_to_rst = 0x0d,
		.rst_to_cke = 0x20,
		.refsel = 1,         /* 32kHz refresh cycles */
		.refr = 7,           /* 8 refresh commands per refresh cycle */
	};

	// solo core soc
	if (is_cpu_type(MXC_CPU_MX6SOLO)) {
		sysinfo.walat = 1;
		puts("SoloCore\n");
		mx6sdl_dram_iocfg(width, &mx6dl_ddr_ioregs, &mx6sdl_grp_ioregs);

		switch(memsz) {

		case 512:
			mx6_dram_cfg(&sysinfo, &mx6s_2g_mmcd_calib, &mem_ddr_2g);
			break;
		default:
			mx6_dram_cfg(&sysinfo, &mx6s_mmcd_calib, &mem_ddr_4g);
			break;
		}
	}

	// dual lite soc
	if (is_cpu_type(MXC_CPU_MX6DL)) {
		puts("DualLite\n");
		sysinfo.walat = 1;
		mx6sdl_dram_iocfg(width, &mx6dl_ddr_ioregs, &mx6sdl_grp_ioregs);

		switch(memsz) {

		case 2048:
			mx6_dram_cfg(&sysinfo, &mx6dl_2g_mmcd_calib, &mem_ddr_4g);
			break;
		default:
			mx6_dram_cfg(&sysinfo, &mx6dl_mmcd_calib, &mem_ddr_2g);
			break;
		}
	}

	// dual or quad soc
	if (is_cpu_type(MXC_CPU_MX6Q) || is_cpu_type(MXC_CPU_MX6D)) {

		if (is_cpu_type(MXC_CPU_MX6Q))
			puts("QuadCore\n");
		if (is_cpu_type(MXC_CPU_MX6D))
			puts("DualCore\n");


		mx6dq_dram_iocfg(width, &mx6q_ddr_ioregs, &mx6q_grp_ioregs);

		switch(memsz) {

		case 4096:
			sysinfo.walat = 1;
			sysinfo.cs_density = 16;
			sysinfo.ncs = 2;
			mx6_dram_cfg(&sysinfo, &mx6q_4g_mmcd_calib, &mem_ddr_8g);
			break;
		case 2048:
			mx6_dram_cfg(&sysinfo, &mx6q_2g_mmcd_calib, &mem_ddr_4g);
			break;
		default:
			mx6_dram_cfg(&sysinfo, &mx6q_mmcd_calib, &mem_ddr_2g);
			break;
		}
	}
}

