// SPDX-License-Identifier:	GPL-2.0+
/*
 * Implements conga-QMX6 LVDS and HDMI handling
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#include <common.h>
#include <init.h>
#include <asm/io.h>
#include <asm-generic/gpio.h>
#include <asm/arch/clock.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/crm_regs.h>
#include <asm/arch/mxc_hdmi.h>
#include <asm/mach-imx/video.h>
#include <dm/device.h>
#include <dm/uclass.h>
#include <power/regulator.h>
#include <env.h>
#include <backlight.h>

/* forward */
static void _do_enable_lvds(struct display_info_t const *dev);
static void _do_enable_hdmi(struct display_info_t const *dev);

/* One LVDS and a HDMI display available */
struct display_info_t const displays[] = {
{

	.di = 0,
	.bus = -1,
	.addr = -1,
	.pixfmt = IPU_PIX_FMT_RGB666,
	.detect = NULL,
	.enable = _do_enable_lvds,
	.mode = {
		.name =
		"Hannstar-XGA",
		.refresh = 60,
		.xres = 1024,
		.yres = 768,
		// .pixclock = 15385, // 65MHz in ps
		.pixclock = 15384, // 65002600Hz in ps
		.left_margin = 220,
		.right_margin = 40,
		.upper_margin = 21,
		.lower_margin = 7,
		.hsync_len = 60,
		.vsync_len = 10,
		.sync = FB_SYNC_EXT,
		.vmode = FB_VMODE_NONINTERLACED }
	},
{
	.di = 0,
	.bus = -1,
	.addr = -1,
	.pixfmt = IPU_PIX_FMT_RGB24,
	.detect = NULL,
	.enable = _do_enable_hdmi,
	.mode = {
		.name = "HDMI",
		.refresh = 60,
		.xres = 1024,
		.yres = 768,
		.pixclock = 15385,
		.left_margin = 220,
		.right_margin = 40,
		.upper_margin = 21,
		.lower_margin = 7,
		.hsync_len = 60,
		.vsync_len = 10,
		.sync = FB_SYNC_EXT,
		.vmode = FB_VMODE_NONINTERLACED }
	 }
};

size_t display_count = ARRAY_SIZE(displays);


/* If PWM is disabled then configure LVDS backlight pins to outputs and set to 1 */
#if ! (defined(CONFIG_PWM_IMX) && defined(CONFIG_BACKLIGHT_PWM))

static void _gpio_set_out_active(const char* const gpio_name, const char* const gpio_descr)
{
	struct gpio_desc desc;
	int ret;

	ret = dm_gpio_lookup_name(gpio_name, &desc);
	if (ret)
		return;

	ret = dm_gpio_request(&desc, gpio_descr);
	if (ret)
		return;

	dm_gpio_set_dir_flags(&desc, GPIOD_IS_OUT | GPIOD_IS_OUT_ACTIVE);
}
#endif


/* Switch backlight on */
static void _enable_lvds_backlight(void)
{
	struct udevice * dev;
	int ret;

#if defined(CONFIG_PWM_IMX) && defined(CONFIG_BACKLIGHT_PWM)

	/* get backlight level from environment, default is 80% */
	unsigned int backlight_level = 80;
	char const *backlight_level_string = env_get("backlight_level");

	if (backlight_level_string) {
		backlight_level = simple_strtoul(backlight_level_string, NULL, 10);
		if (backlight_level > 100)
			backlight_level = 100;
	}

	/* set backlight level and enable it */
	ret = uclass_get_device_by_name(UCLASS_PANEL_BACKLIGHT, "lvds0_backlight", &dev);

	if (ret) {
		printf("WARNING: Cannot get backlight from DT: ERR=%d\n", ret);
		return;
	}
	backlight_set_brightness(dev, backlight_level);
	backlight_enable(dev);

#else
	/* if no PWM is enabled, we have to switch on power and backlight manually */
	ret = uclass_get_device_by_name(UCLASS_REGULATOR, "reg_lvds_power", &dev);
	if (ret) {
		printf("WARNING: Cannot get LVDS power regulator \"reg_lvds_power\" from DT: ERR=%d\n", ret);
		return;
	}
	ret = regulator_set_enable(dev, true);
	if (ret) {
		printf("WARNING: Cannot enable regulator for LVDS '%s'\n", dev->name);
	}

	/* switch PWM backlight on if no PWM is used */
	/* pads already configured in DT pinconfig groups for pwm backlight and pwm */
	_gpio_set_out_active("GPIO1_09","LVDS Backlight Enable");
	_gpio_set_out_active("GPIO1_18","LVDS PWM");
#endif
}


/* Set video-PLL to 455MHz and enable it */
static void _enable_videopll(void)
{
	struct mxc_ccm_reg *ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;
	int reg;
	s32 timeout = 100000;

	/* powerdown PLL5 clock */
	reg = readl(&ccm->analog_pll_video);
	reg |= BM_ANADIG_PLL_VIDEO_POWERDOWN;
	writel(reg, &ccm->analog_pll_video);


	/* set PLL5 to 455 MHz */
	/* will be later divided by 7 (cscmr2) => 65 MHz */
	/* PLL output frequency = 24MHz * (DIV_SELECT + NUM/DENOM) / POST_DIV */
	reg &= ~BM_ANADIG_PLL_VIDEO_DIV_SELECT;
	reg |= BF_ANADIG_PLL_VIDEO_DIV_SELECT(37);
	reg &= ~BM_ANADIG_PLL_VIDEO_POST_DIV_SELECT;
	reg |= BF_ANADIG_PLL_VIDEO_POST_DIV_SELECT(1); // div by 2
	writel(reg, &ccm->analog_pll_video);

	writel(BF_ANADIG_PLL_VIDEO_NUM_A(11), &ccm->analog_pll_video_num);
	writel(BF_ANADIG_PLL_VIDEO_DENOM_B(12),&ccm->analog_pll_video_denom);

	/* powerup PLL5 and wait until PLL locked */
	reg &= ~BM_ANADIG_PLL_VIDEO_POWERDOWN;
	writel(reg, &ccm->analog_pll_video);

	while (timeout--)
		if (readl(&ccm->analog_pll_video) & BM_ANADIG_PLL_VIDEO_LOCK)
			break;
	if (timeout <= 0)
		printf("Warning: video pll lock timeout!\n");

	/* enable PLL output and ensure bypass is not set */
	reg = readl(&ccm->analog_pll_video);
	reg |= BM_ANADIG_PLL_VIDEO_ENABLE;
	reg &= ~BM_ANADIG_PLL_VIDEO_BYPASS;
	writel(reg, &ccm->analog_pll_video);
}


/* Configure LDB, i.e set clock to PLL5 and 65MHz output */
/* Also use LDB clock for IPU1-DI0 (IPU1 is the first IPU) */
static void _enable_ldb(void)
{
	struct mxc_ccm_reg *ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;
	int reg;

	/* Turn on LDB-DI0 clock */
	reg = readl(&ccm->CCGR3);
	reg |=  MXC_CCM_CCGR3_LDB_DI0_MASK;
	writel(reg, &ccm->CCGR3);

	/* set LDB0 clk select to 000 (PLL5 clock) */
	reg = readl(&ccm->cs2cdr);
	reg &= ~MXC_CCM_CS2CDR_LDB_DI0_CLK_SEL_MASK;
	reg |= (0 << MXC_CCM_CS2CDR_LDB_DI0_CLK_SEL_OFFSET);
	writel(reg, &ccm->cs2cdr);

	/* Divide LDB clock by 7 for IPU1-DI0 clock input */
	reg = readl(&ccm->cscmr2);
	reg |= MXC_CCM_CSCMR2_LDB_DI0_IPU_DIV;
	writel(reg, &ccm->cscmr2);

	/* Select LDB-DI0 clock for IPU-DI0 */
	/* default divider for clock is set to 3 */
	reg = readl(&ccm->chsccdr);
	reg &= ~(MXC_CCM_CHSCCDR_IPU1_DI0_CLK_SEL_MASK << MXC_CCM_CHSCCDR_IPU1_DI0_CLK_SEL_OFFSET);
	reg |= (CHSCCDR_CLK_SEL_LDB_DI0 << MXC_CCM_CHSCCDR_IPU1_DI0_CLK_SEL_OFFSET);
	writel(reg, &ccm->chsccdr);
}


/* Disable LVDS display clock */
static void _disable_lvds(void)
{
	struct iomuxc *iomux = (struct iomuxc *)IOMUXC_BASE_ADDR;
	int reg;

	/* After a switchover from HDMI to LVDS backlight
	   via a "reset" command the LVDS display clock is
	   still active because GPR2 register is not cleared on a
	   softreboot eg via "reset" command.
	   So disable LVDS output explicitely
	*/
	reg = readl(&iomux->gpr[2]);
	reg &= ~(IOMUXC_GPR2_LVDS_CH0_MODE_MASK | IOMUXC_GPR2_LVDS_CH1_MODE_MASK);
	writel(reg, &iomux->gpr[2]);
}


/* Configure LVDS panel and route IPU1-DI0 to LVDS0 */
static void _do_enable_lvds(struct display_info_t const *dev)
{
	struct iomuxc *iomux = (struct iomuxc *)IOMUXC_BASE_ADDR;
	int reg;

	/* Configure LVDS panel output */
	reg = IOMUXC_GPR2_BGREF_RRMODE_EXTERNAL_RES
	      | IOMUXC_GPR2_DI1_VS_POLARITY_ACTIVE_HIGH
	      | IOMUXC_GPR2_DI0_VS_POLARITY_ACTIVE_HIGH
	      | IOMUXC_GPR2_BIT_MAPPING_CH0_SPWG
	      | IOMUXC_GPR2_DATA_WIDTH_CH0_18BIT
	      | IOMUXC_GPR2_LVDS_CH0_MODE_ENABLED_DI0
	      | IOMUXC_GPR2_LVDS_CH1_MODE_DISABLED;
	writel(reg, &iomux->gpr[2]);

	/* route IPU1-DI0 to LVDS0 */
	reg = readl(&iomux->gpr[3]);
	reg &= ~IOMUXC_GPR3_LVDS0_MUX_CTL_MASK;
	reg |= (IOMUXC_GPR3_MUX_SRC_IPU1_DI0 << IOMUXC_GPR3_LVDS0_MUX_CTL_OFFSET);
	writel(reg, &iomux->gpr[3]);

	/* switch on backlight */
	_enable_lvds_backlight();
}


/* Route IPU1-DI0 to HDMI output */
static void _do_enable_hdmi(struct display_info_t const *dev)
{
	struct iomuxc *iomux = (struct iomuxc *)IOMUXC_BASE_ADDR;
	int reg;

	_disable_lvds();

	/* route IPU1-DI0 to HDMI */
	reg = readl(&iomux->gpr[3]);
	reg &= ~IOMUXC_GPR3_HDMI_MUX_CTL_MASK;
	reg |= (IOMUXC_GPR3_MUX_SRC_IPU1_DI0 << IOMUXC_GPR3_HDMI_MUX_CTL_OFFSET);
	writel(reg, &iomux->gpr[3]);

	/* enable hdmi phy at last */
	imx_enable_hdmi_phy();
}


/* Configure LVDS and HDMI display
 * Before this methodis called, ensure that PLL5 is
 * already configured in eg. board_early_init_f with
 * "select_ldb_di_clock_source(MXC_PLL5_CLK);""
 */
void qmx6_display_setup(void)
{
	enable_ipu_clock();
	imx_setup_hdmi();
	_enable_videopll();
	_enable_ldb();
}
