// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#include <errno.h>
#include <env.h>
#include <asm/arch/sys_proto.h>
#include <asm-generic/gpio.h>
#include <power/pmic.h>
#include <power/pfuze100_pmic.h>

#include "qmx6_pmic.h"


struct interface_level {
	char *name;
	uchar value;
};

static struct interface_level mipi_levels[] = {
	{"0V0", 0x00},
	{"2V5", 0x17},
};


/* check bypass register according to:
 *  Bits  0- 4: regulator ARM:     0x1F = bypass = REG0_TARG
 *  Bits  9-13: regulator VPU/GPU: 0x00 = off    = REG1_TARG
 *  Bits 18-22: regulator SOC:     0x1F = bypass = REG2_TARG
*/
#define REG_VAL_IF_BYPASS 0x007C001F
static int _verify_anatop_bypass(void)
{
	struct anatop_regs *anatop = (struct anatop_regs *)ANATOP_BASE_ADDR;
	u32 reg = readl(&anatop->reg_core);

	if (REG_VAL_IF_BYPASS == reg)
	{
		printf("Switch to ldo_bypass mode!\n");
		return 0;
	}
	else
	{
		printf("ERROR: ldo_bypass not enabled: PMU_REG_COREn (0x%p"
			") = %08x (expected: %08x)\n", &anatop->reg_core, reg, REG_VAL_IF_BYPASS);
		return -EIO;
	}
}



#ifdef CONFIG_DM_PMIC

/* Init PMIC (Driver Model Version) */
int qmx6_pmic_init(void)
{
	struct udevice *dev;
	int ret, i;
	unsigned int dev_id, rev_id;
	char const *lv_mipi;

	/* i2c mux already configured via driver model (DM) */
	ret = pmic_get("pfuze100@8", &dev);
	if (ret == -ENODEV) {
		printf("No PMIC found!\n");
		return -ENODEV;
	}

	dev_id = pmic_reg_read(dev, PFUZE100_DEVICEID);
	rev_id = pmic_reg_read(dev, PFUZE100_REVID);
	printf("PFUZE100 Rev. [0x%02x/0x%02x] detected\n", dev_id, rev_id);

	/* directly set ldo to bypass mode */
	qmx6_ldo_set_bypass();

	if (rev_id >= 0x20)
	{
		return 0;
	}

	/* set level of MIPI if specified */
	lv_mipi = env_get("lv_mipi");
	if (lv_mipi)
	{
		return 0;
	}

	for (i = 0; i < ARRAY_SIZE(mipi_levels); i++) {
		if (!strcmp(mipi_levels[i].name, lv_mipi)) {
			printf("set MIPI level %s\n", mipi_levels[i].name);
			ret = pmic_reg_write(dev, PFUZE100_VGEN4VOL,
					     mipi_levels[i].value);
			if (ret)
			{
				printf("ERROR: pmic register write for mipi level %d (value: %d)\n", i, mipi_levels[i].value);
				return ret;
			}
		}
	}

	return 0;
}

/* Set Anatop to bypass and also configure PMIC (Driver Model Version) */
/* This implementation is based on sabresd PMIC init */
void qmx6_ldo_set_bypass(void)
{
	struct udevice *dev;
	int ret;
	int is_400M;
	u32 vddarm;
	int ldo_bypass = 1;

	ret = pmic_get("pfuze100@8", &dev);
	if (ret == -ENODEV)
		return;

	/* increase VDDARM/VDDSOC to support 1.2G chip */
	if (check_1_2G()) {
		ldo_bypass = 0; /* ldo_enable on 1.2G chip */
		printf("1.2G chip, increase VDDARM_IN/VDDSOC_IN\n");
		if (is_mx6dqp()) {
			/* increase VDDARM to 1.425V */
			pmic_clrsetbits(dev, PFUZE100_SW2VOL, 0x3f, 0x29);
		} else {
			/* increase VDDARM to 1.425V */
			pmic_clrsetbits(dev, PFUZE100_SW1ABVOL, 0x3f, 0x2d);
		}
		/* increase VDDSOC to 1.425V */
		pmic_clrsetbits(dev, PFUZE100_SW1CVOL, 0x3f, 0x2d);
	}

	/* switch to ldo_bypass mode, boot on 800Mhz */
	if (ldo_bypass) {
		prep_anatop_bypass();
		if (is_mx6dqp()) {
			/* decrease VDDARM for 400Mhz DQP:1.1V*/
			pmic_clrsetbits(dev, PFUZE100_SW2VOL, 0x3f, 0x1c);
		} else {
			/* decrease VDDARM for 400Mhz DQ:1.1V, DL:1.275V */
			if (is_mx6dl())
				pmic_clrsetbits(dev, PFUZE100_SW1ABVOL, 0x3f, 0x27);
			else
				pmic_clrsetbits(dev, PFUZE100_SW1ABVOL, 0x3f, 0x20);
		}
		/* increase VDDSOC to 1.3V */
		pmic_clrsetbits(dev, PFUZE100_SW1CVOL, 0x3f, 0x28);

		/*
		 * MX6Q/DQP:
		 * VDDARM:1.15V@800M; VDDSOC:1.175V@800M
		 * VDDARM:0.975V@400M; VDDSOC:1.175V@400M
		 * MX6DL:
		 * VDDARM:1.175V@800M; VDDSOC:1.175V@800M
		 * VDDARM:1.15V@400M; VDDSOC:1.175V@400M
		 */
		is_400M = set_anatop_bypass(2);
		if (is_mx6dqp()) {
			if (is_400M)
				pmic_clrsetbits(dev, PFUZE100_SW2VOL, 0x3f, 0x17);
			else
				pmic_clrsetbits(dev, PFUZE100_SW2VOL, 0x3f, 0x1e);
		}

		if (is_400M) {
			if (is_mx6dl())
				vddarm = 0x22;
			else
				vddarm = 0x1b;
		} else {
			if (is_mx6dl())
				vddarm = 0x23;
			else
				vddarm = 0x22;
		}
		pmic_clrsetbits(dev, PFUZE100_SW1ABVOL, 0x3f, vddarm);

		/* decrease VDDSOC to 1.175V */
		pmic_clrsetbits(dev, PFUZE100_SW1CVOL, 0x3f, 0x23);

		/* set operating frequency back to 800 MHz */
		finish_anatop_bypass();

		_verify_anatop_bypass();
	}
}

#else

/* Init PMIC (non DM-Version) */
int qmx6_pmic_init(void)
{
	struct pmic *p;
	u32 dev_id, rev_id, i;
	int ret;
	char const *lv_mipi;

	/* configure I2C multiplexer */
	/* enable PMIC i2c bus (i2c4) statically */
	gpio_request(CGTQMX6_PFUZE_I2C_MUX_GPIO, "I2C1 bus mux for PMIC");
	gpio_direction_output(CGTQMX6_PFUZE_I2C_MUX_GPIO, 1);

	power_pfuze100_init(CGTQMX6_PFUZE_I2C_BUS);
	p = pmic_get("PFUZE100");
	if (!p)
		return -EINVAL;

	ret = pmic_probe(p);
	if (ret)
		return ret;

	pmic_reg_read(p, PFUZE100_DEVICEID, &dev_id);
	pmic_reg_read(p, PFUZE100_REVID, &rev_id);
	printf("PFUZE100 Rev. [0x%02x/0x%02x] detected\n", dev_id, rev_id);

	/* directly set ldo to bypass mode */
	qmx6_ldo_set_bypass();

	if (rev_id >= 0x20)
	{
		return 0;
	}

	/* set level of MIPI if specified */
	lv_mipi = env_get("lv_mipi");
	if (lv_mipi)
	{
		return 0;
	}

	for (i = 0; i < ARRAY_SIZE(mipi_levels); i++) {
		if (!strcmp(mipi_levels[i].name, lv_mipi)) {
			printf("set MIPI level %s\n", mipi_levels[i].name);
			ret = pmic_reg_write(p, PFUZE100_VGEN4VOL,
					     mipi_levels[i].value);
			if (ret)
			{
				printf("ERROR: pmic register write for mipi level %d (value: %d)\n", i, mipi_levels[i].value);
				return ret;
			}
		}
	}

	return 0;
}


/* Set Anatop to bypass and also configure PMIC (non DM-Version) */
/* This implementation is based on sabresd PMIC init */
void qmx6_ldo_set_bypass(void)
{
	unsigned int value;
	int is_400M;
	unsigned char vddarm;
	int ldo_bypass = 1;
	struct pmic *p = pmic_get("PFUZE100");

	if (!p) {
		printf("No PMIC found!\n");
		return;
	}

	/* increase VDDARM/VDDSOC to support 1.2G chip */
	if (check_1_2G()) {
		ldo_bypass = 0;	/* ldo_enable on 1.2G chip, bypass not allowed */
		printf("1.2G chip, increase VDDARM_IN/VDDSOC_IN\n");
		if (is_mx6dqp()) {
			/* increase VDDARM to 1.425V */
			pmic_reg_read(p, PFUZE100_SW2VOL, &value);
			value &= ~0x3f;
			value |= 0x29;  // 0.4 + 0x29 * 0.025V = 1.425V
			pmic_reg_write(p, PFUZE100_SW2VOL, value);
		} else {
			/* increase VDDARM to 1.425V */
			pmic_reg_read(p, PFUZE100_SW1ABVOL, &value);
			value &= ~0x3f;
			value |= 0x2d; // 0.3 + 0x2d * 0.025 = 1.425
			pmic_reg_write(p, PFUZE100_SW1ABVOL, value);
		}
		/* increase VDDSOC to 1.425V */
		pmic_reg_read(p, PFUZE100_SW1CVOL, &value);
		value &= ~0x3f;
		value |= 0x2d; // 0.3 + 0x2d * 0.025 = 1.425
		pmic_reg_write(p, PFUZE100_SW1CVOL, value);
	}

	/* switch to ldo_bypass mode, boot on 800Mhz */
	if (ldo_bypass) {
		// switch CPU to 400MHz for Anatop/PMIC switchover
		// also set LDO voltage to 0.97V (MX6SDL) / 1.15V (MX6QDL)
		prep_anatop_bypass();

		if (is_mx6dqp()) {
			/* decrease VDDARM for 400Mhz DQP:1.1V*/
			pmic_reg_read(p, PFUZE100_SW2VOL, &value);
			value &= ~0x3f;
			value |= 0x1c;   // 0.4 + 0x1c * 0.025V = 1.1V
			pmic_reg_write(p, PFUZE100_SW2VOL, value);
		} else {
			/* decrease VDDARM for 400Mhz DQ:1.1V, DL:1.275V */
			pmic_reg_read(p, PFUZE100_SW1ABVOL, &value);
			value &= ~0x3f;
			if (is_mx6dl())
				value |= 0x27; // 0.3 + 0x27 * 0.025V = 1.275
			else
				value |= 0x20; // 0.3 + 0x20 * 0.025V = 1.1

			pmic_reg_write(p, PFUZE100_SW1ABVOL, value);
		}
		/* increase VDDSOC to 1.3V */
		pmic_reg_read(p, PFUZE100_SW1CVOL, &value);
		value &= ~0x3f;
		value |= 0x28; // 0.3 + 0x28 * 0.025V
		pmic_reg_write(p, PFUZE100_SW1CVOL, value);

		/*
		 * MX6Q/DQP:
		 * VDDARM:1.15V@800M; VDDSOC:1.175V@800M
		 * VDDARM:0.975V@400M; VDDSOC:1.175V@400M
		 * MX6DL:
		 * VDDARM:1.175V@800M; VDDSOC:1.175V@800M
		 * VDDARM:1.15V@400M; VDDSOC:1.175V@400M
		 */

		/* switchover from Anatop -> PMIC */
		/* also disable the watchdog on uboot */
		is_400M = set_anatop_bypass(0);

		/* adjust SW2VOL (VDDHIGH) on quad-plus/dual-plus */
		if (is_mx6dqp()) {
			pmic_reg_read(p, PFUZE100_SW2VOL, &value);
			value &= ~0x3f;
			if (is_400M)
				value |= 0x17; // 0.4 + 0x17*0.025V = 0.975V
			else
				value |= 0x1e; // 0.4 + 0x1e*0.025V = 1.15V
			pmic_reg_write(p, PFUZE100_SW2VOL, value);
		}

		/* adjust PMIC VDDARM; depends on operating frequency and core type */
		if (is_400M) {
			if (is_mx6dl())
				vddarm = 0x22; // 0.3 + 0x22 * 0.025V = 1.15V
			else
				vddarm = 0x1b; // 0.3 + 0x1b * 0.025V = 0.95V
		} else {
			/* VDDARM to 1.175V for all CGTQMX6 variants at 800 MHz */
			vddarm = 0x23; // 0.3 + 0x23 * 0.025V = 1.175V
		}
		pmic_reg_read(p, PFUZE100_SW1ABVOL, &value);
		value &= ~0x3f;
		value |= vddarm;
		pmic_reg_write(p, PFUZE100_SW1ABVOL, value);

		/* decrease VDDSOC to 1.175V */
		pmic_reg_read(p, PFUZE100_SW1CVOL, &value);
		value &= ~0x3f;
		value |= 0x23;
		pmic_reg_write(p, PFUZE100_SW1CVOL, value);

		/* set operating frequency back to 800 MHz */
		finish_anatop_bypass();

		_verify_anatop_bypass();
	}
}

#endif
