/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#ifndef __QMX6_VIDEO_H
#define __QMX6_VIDEO_H

/* Enable a display on conga-QMX6 modules
 *  - uses IPU#1 and DI0
 *  - routes either to LVDS#0 or HDMI
 */
void qmx6_display_setup(void);


#endif // __QMX6_VIDEO_H