/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#ifndef __QMX6_PMIC_H
#define __QMX6_PMIC_H


/*
 * PMIC and Anatop functions
 *
 * PMIC can be used with or without uboot driver model (DM)
 * If enabled (CONFIG_DM_PMIC and DM_PMIC_PFUZE100 are set),
 * enusre that DM_GPIO, DM_I2C, I2C_MUX, I2C_MUX_GPIO
 * are also enabled
 */


/* Configure PMIC */
int qmx6_pmic_init(void);

/* Set Anatop to bypass and configure pmic accordingly */
void qmx6_ldo_set_bypass(void);

#endif // __QMX6_PMIC_H