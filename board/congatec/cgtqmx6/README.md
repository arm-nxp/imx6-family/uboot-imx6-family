# Bootloader for conga-QMX6 modules

## 1. Overview
This repository contains the bootloader for all standard **conga-QMX6** modules.

It is derived from NXP bootloader **U-Boot v2022.04**.
For building instructions please refer to the reference manual for NXP i.MX6 reference
platform (i.MX6 quad sabresd) and also check section [4. Build](#4-build) for details.


-------
## 2. Changes to previous version (u-boot 2016)

### 2.1 u-boot environment location
Please be aware that the environment location has changed on SPI flash. It is now at the very end of the bootloader partition, ie. at (1 MiB - 8 KiB) with a size of 8 KiB. On uboot2016 it was on fixed offset of 768 KiB.

This shouldn't lead to any issues, because a bootloader update erases the full bootloader partition. With this, also the environment is reset to its default values.


### 2.2 Kernel device tree names
With the new uboot also a new BSP with Kernel 5.15 is supported. In this Kernel release the device tree names for conga-QMX6 modules have changed from eg. "imx6q-qmx6.dtb" to "imx6q-cgtqmx6.dtb" to have a direct identification from filename to module.

So if the new bootloader should boot an old BSP, please update the environment manually.

### 2.3 PCI-e
As from this release, PCIe support is enabled by default. It can be disabled in defconfig by removing the switches CONFIG_PCI and CONFIG_CMD_PCI or via make menuconfig.


-------
## 3. Additional features:


### 3.1 LVDS/HDMI Display support

#### 3.1.1 Display on U-Boot
Add CONFIG_CGTQMX6_DISPLAY=y to defconfig if an LVDS display or HDMI should be used. Currently supported display outputs on uboot are HDMI and a 1024x768 LVDS display.

After bootup with enabled CONFIG_CGTQMX6_DISPLAY the LVDS display is used as primary output. To switch to HDMI, set the panel environment variable to "HDMI", eg:

```sh
=> setenv panel HDMI
=> savee
=> reset
```
After this, the HDMI port is used. Switch back to LVDS by setting the panel variable to "Hannstar-XGA"

#### 3.1.2 Display on Kernel

The Kernel provided by congatec has by default an enabled HDMI output as main display
and 1024x768 LVDS screen as secondary (which is not configured in Wayland).
Enabling the secondary screen is done by unblanking the appropriate framebuffer device:
```sh
  echo 0 > /sys/class/graphics/fb2/blank
```

Change display configuration via kernel command line on uboot, eg:
 - set first display to LVDS (use only LVDS)
```sh
   => setenv displayinfo "video=mxcfb0:dev=ldb,if=RGB666,fbpix=BGR32"
```
 - set first display to LVDS and HDMI as secondary
```sh
   => setenv displayinfo "video=mxcfb0:dev=ldb,if=RGB666,fbpix=BGR32 video=mxcfb1:dev=hdmi,1920x1080M@60,if=RGB24,fbpix=BGR32"
```

This is independent of conga-QMX6 hardware revision. The usage of **displayinfo** environment
variable is described in IMX_LINUX_USERS_GUIDE.

The LVDS (LDB) configuration on kernel commandline always references to the specified timings node in the device tree.
These timings node have to be adjusted to use other LVDS displays as the default one which is 1024x768 (18-Bits).
For this, there are some example device trees available with different LVDS/HDMI outputs:

 - `imx6q-cgtqmx6-hdmi.dts`: Only HDMI is enabled, all other LDB-devices/framebuffers are disabled
 - `imx6q-cgtqmx6-lvds.dts`: Only LVDS is enabled, HDMI disabled completely
 - `imx6q-cgtqmx6-lvds-1024x768-24.dts`: Like above, but 24-bits LVDS pixel interface
 - `imx6q-cgtqmx6-lvds-800x600.dts`: Like above, but 800x600 display resolution
 - `imx6q-cgtqmx6-1280x1024x2.dts`: Like above, but 1280x1024 display resolution (dualchannel interface)
 - `imx6q-cgtqmx6-dualview.dts`: Two independent LVDS displays with different resolutions
 - `imx6q-cgtqmx6-dualview-hdmi.dts`: As above, plus HDMI as 3rd independent display

**Remark:**
Only Dual-/Quadcore DT's listed above as an example (eg: `imx6q-cgtqmx6-hdmi.dts`) but they are also available for SoloCore/DualLite (eg: `imx6dl-cgtqmx6-hdmi.dts`) and for hardware revision E.x (eg: `imx6q-cgtqmx6-hdmi-revE.dts`)

As an additional example DT there is `imx6q-cgtqmx6-pwm.dts` which uses HDMI as output and enables all 4 channels of PWM outputs.



### 3.2 SATA
SATA is only available on i.MX6 Dual- or QuadCore CPU's and not on Solo/DualLite.

To enable SATA add the following config switches to cgtqmx6_defconfig:

    CONFIG_AHCI=y
    CONFIG_SCSI=y
    CONFIG_DM_SCSI=y
    CONFIG_IMX_AHCI=y
    CONFIG_CMD_SCSI=y

Afterwards the scsi command on uboot is available, eg:

```sh
=> scsi reset
=> scsi info
```

To boot from SATA set the following environment options:
```sh
setenv scsidev 0
setenv scsipart 1
setenv scsiroot '/dev/sda1 rootwait rw'
setenv loadfdtscsi 'echo Loading ${fdt_file}; ext4load scsi ${scsidev}:${scsipart} ${fdt_addr} boot/${fdt_file}'
setenv scsiargs 'setenv bootargs console=${console},${baudrate} ${smp} earlycon root=${scsiroot} ${displayinfo}'
setenv scsiboot 'echo Booting from scsi ...; run scsiargs; run loadfdtscsi; bootm ${loadaddr} - ${fdt_addr}'
setenv loadimagescsi 'echo Loading ${image}; ext4load scsi ${scsidev}:${scsipart} ${loadaddr} boot/${image}'
setenv bootcmd 'run findfdt; scsi reset; scsi dev ${scsidev}; run loadimagescsi; run scsiboot'
```


### 3.3 PCIe
PCIe is enabled by default but drivers for various cards are not necessarily included.

#### 3.3.1 Use PCIe Intel E1000 as second network interface
The driver for the Intel E1000 network card has to be enabled by adding the following line to cgtqmx6_defconfig:

    CONFIG_E1000=y

Then build the bootloader as described in section [4. Build](#4-build) and flash it to the board.

Check if NIC is available with "pci" command. It should look like this
```sh
=> pci
BusDevFun  VendorId   DeviceId   Device Class       Sub-Class
_____________________________________________________________
00.00.00   0x16c3     0xabcd     Bridge device           0x04
01.00.00   0x8086     0x10d3     Network controller      0x00
```

Check available and active NIC's with
```sh
=> net list
```


To use this card as primary network interface in bootloader environment, configure bootloader network subsystem as follows:
```sh
=> pci enum
=> setenv ethprime eth1
=> setenv ethact e1000#0
=> dhcp
```



### 3.4 Non-SPL bootloader
The default bootloader for all standard conga-QMX6 modules is a SPL bootloader. This SPL bootloader supports all standard conga-QMX6 modules. Non-SPL bootloaders for the different modules are also supported.

Be aware, that the non-SPL bootloader is variant specific and has to be configured properly for the module to use with.

Please see section [4. Build](#4-build) for details.

### 3.5 HAB/TEE
Not supported in this version.

### 3.6 Falcon Mode
Not supported in this version.


-------
## 4. Build

### 4.1 SPL Bootloader (default)
The SPL bootloader is the default bootloader to use.
It fits for every standard conga-QMX6 module and could be used for
flashing as well as booting with uuu tool.
The file(s) to use depends on its usage:
 - firmware to boot with uuu: **"u-boot-with-spl.imx"**
 - firmware to flash with uuu: **"SPL"** and **"u-boot.img"**

As SPL bootloader is the default bootloader it can be simply
built (after sourcing the arm cross-toolchain) with:
```sh
  $ make cgtqmx6_defconfig
  $ make
```

### 4.2 Non-SPL Bootloader
The non-SPL bootloader has to be built exactly for the used module. They differs in SOC type, memory size, memory width and memory speed.

 - **SOC Type:** SDL (SoloCore/DualLite) or QDC (Quad-/DualCore)
 - **Memory Size:** 1024, 2048, 4096 (MiB)
 - **Memory Width:** 32, 64 (Bits)
 - **Memory Speed:** 400, 528 (MHz)

**Remark:**
 - Memory speed is related to SOC type: i.MX6 SoloCore and DualLite: 400MHz, Dual-/QuadCore: 528MHz
 - Memory width: on modules with a memory width of 32-Bits only one RAM chip is populated (the second one is empty)


**Select the non-SPL bootloader**

As SOC type and memory speed are related, only SOC type have to be specified from the differences above.
For standard conga-QMX6 modules the following defconfigs are provided:

| defconfig | valid module revision | valid standard module PN's |
|-|-|-|
| `cgtqmx6_nonspl_QDC_1024_64_defconfig` | B.x, C.x | 016102, 016103, 016112, 016113, 016302, 016303, 016312, 016313 |
| `cgtqmx6_nonspl_QDC_2048_64_defconfig` | B.x, C.x | 016104, 016304 |
| `cgtqmx6_nonspl_QDC_4096_64_defconfig` | B.x, C.x | 016318 |
| `cgtqmx6_nonspl_SDL_1024_32_defconfig` | B.x, C.x | 016100, 016110, 016300, 016310 |
| `cgtqmx6_nonspl_SDL_1024_64_defconfig` | B.x, C.x | 016101, 016111, 016301, 016311 |
| `cgtqmx6_revE_nonspl_QDC_1024_64_defconfig` | E.x | 016302, 016303, 016312,016313 |
| `cgtqmx6_revE_nonspl_QDC_2048_64_defconfig` | E.x | 016304 |
| `cgtqmx6_revE_nonspl_QDC_4096_64_defconfig` | E.x | 016318 |
| `cgtqmx6_revE_nonspl_SDL_1024_32_defconfig` | E.x | 016300,016310 |
| `cgtqmx6_revE_nonspl_SDL_1024_64_defconfig` | E.x | 016301,016311 |


**Build**

Use the one which fits to your module, eg. for a conga-QMX6 SoloCore with PN016100 do
```sh
$ make cgtqmx6_nonspl_SDL_1024_32_defconfig
$ make
```


**Remark:**
Do not use `make menuconfig` to change memory configuration of the non-SPL bootloader! Some dependent configuration options (ie. the RAM config DCD file and the device tree) aren't updated correctly if the config switches aren't set from the initial `make cgtqmx6_nonspl_<module_type>_defconfig` on.


-------
## 5. Flash Bootloader
Prior to flashing the firmware, the backplane has to be configured to boot in recovery mode. Then connect the recovery USB port of the backplane to your
development station.

Flashing the firmware to the conga-QMX6 module is then done via uuu tool which is provided by NXP. Use uuu tool with version 1.4.224 and above. This is necessary, because NXP changed USB vendor-id to 0x1fc9 and the product-id to 0x0152. Older uuu versions may not recognise these settings.

[uuu version 1.4.224](https://github.com/NXPmicro/mfgtools/releases/tag/uuu_1.4.224)


For testing purposes it is possible to boot directly with uuu tool if recovery mode is set. Copy **u-boot-with-spl.imx** from uboot build folder into uuu folder and open eg a Windows OS PowerShell console in that folder. Then start uuu:
```
.\uuu.exe -b spl u-boot-with-spl.imx
```


To flash the firmware create a text file named **conga-qmx6-flash-spl.auto** in uuu folder and copy the content from APPENDIX to it. Then start uuu:
```
.\uuu.exe conga-qmx6-flash-spl.auto
```


-------
## 6. APPENDIX

### 6.1 FILE conga-qmx6-flash-spl.auto
```sh
uuu_version 1.2.39

# u-boot-with-spl.imx | boot bootloader with spl
# SPL                 | spl to flash to spi flash
# u-boot.img          | uboot-proper to flash to spi-flash

# This command will be run when i.MX6/7 i.MX8MM, i.MX8MQ
SDP: boot -f u-boot-with-spl.imx

# This command will be run when ROM support stream mode
# i.MX8QXP, i.MX8QM, skip QSPI header
SDPS: boot -f u-boot-with-spl.imx -skipfhdr

# These commands will be run when use SPL and will be skipped if no spl
# SDPU will be deprecated. please use SDPV instead of SDPU
# {
SDPU: delay 1000
SDPU: write -f u-boot-with-spl.imx -offset 0x10000 -skipfhdr
SDPU: jump
# }

# These commands will be run when use SPL and will be skipped if no spl
# if (SPL support SDPV)
# {
SDPV: delay 1000
SDPV: write -f u-boot-with-spl.imx -skipspl -skipfhdr
SDPV: jump
# }

# erase first 1MiB (u-boot partition)
FB: ucmd sf probe
FB: ucmd echo "Flash Erase"
FB[-t 40000]: ucmd sf erase 0 0x100000


FB: ucmd setenv fastboot_buffer ${loadaddr}
FB: download -f SPL

# write SPL to offset 1k
FB: ucmd echo "Flash SPL"
FB[-t 20000]: ucmd sf write ${fastboot_buffer} 0x400 ${fastboot_bytes}

# write u-boot to offset 64k
FB: download -f u-boot.img
FB: ucmd echo "Flash u-boot.img"
FB[-t 30000]: ucmd sf write ${fastboot_buffer} 0x10000 ${fastboot_bytes}

FB: done
```
