// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#include <spi.h>
#include <spi_flash.h>
#include <dm/device-internal.h>

#include "qmx6_mfg.h"


/* manufacturing data struct */
struct cgtmfgdata {
	unsigned char tsize;      /* total size in bytes */
	unsigned char ckcnt;      /* size of checksummed part in bytes */
	unsigned char cksum;      /* checksum corrected byte */
	unsigned char serial[6];  /* decimal serial number, packed BCD */
	unsigned char pn[16];     /* part number, right justified, ASCII */
	unsigned char ean[7];     /* ean code, pcked BCD */
	unsigned char hwrev[2];   /* hardware revision, 1st byte: major, 2nd byte: minor */
	unsigned char spare[30];  /* reserved */
};


#define CGTMFG_SIZE            (sizeof(struct cgtmfgdata))
#define CGTMFG_ADDR_OFFSET     (flash->size - SZ_16K)

/* copy pn left aligned and ensure that it is a valid c-str */
static int _conv_pn_to_str(unsigned char * const dst, const unsigned char * const src, int len)
{
	int remain = len;
	unsigned char *sptr = (unsigned char *) src;
	unsigned char *dptr = dst;
	int cnt = 0;

	while (remain) {
		if (*sptr) {
			*dptr = *sptr;
			dptr++;
			cnt++;
		}
		sptr++;
		remain--;
	}
	*dptr = 0x0;
	return cnt;
}

unsigned char _calc_mfg_chksum(struct cgtmfgdata * const data)
{
	int tmpSum = 0;
	int remain = data->ckcnt;
	unsigned char *ptr = (unsigned char *) data;
	unsigned char tmpStore = data->cksum;

	data->cksum = 0;
	while (remain) {
		tmpSum += *ptr;
		ptr++;
		remain--;
	}

	data->cksum = tmpStore;
	return (256 - (tmpSum%256));
}


/* Returns the total size of the board memory [in MiB] */
int _get_memsize_from_pn(const unsigned char* const pn)
{
	int i;
	int arraysize;

	/* define part numbers that differs from default of 1GiB memory */
	char partnumbers_2g[][7] = { "016104", "016105", "016304", "016305" };
	char partnumbers_4g[][7] = { "016308", "016318" };
	char partnumbers_512m[][7] = { "016203" };

	/* check part numbers for 2GiB of RAM */
	arraysize = ARRAY_SIZE(partnumbers_2g);
	for (i=0; i < arraysize; i++) {
		if (!memcmp(pn,partnumbers_2g[i],6))
			return 2048;
	}

	/* check part numbers for 4GiB of RAM */
	arraysize = ARRAY_SIZE(partnumbers_4g);
	for (i=0; i < arraysize; i++) {
		if (!memcmp(pn,partnumbers_4g[i],6))
			return 4096;
	}

	/* check part numbers for 512MiB of RAM */
	arraysize = ARRAY_SIZE(partnumbers_512m);
	for (i=0; i < arraysize; i++) {
		if (!memcmp(pn,partnumbers_512m[i],6))
			return 512;
	}

	/* default to 1GByte */
	return 1024;
}


int qmx6_mfg_read_data_from_flash(struct spi_flash * flash, struct cgtqmx6_mfg_data * md)
{
	int ret;
	char buf[CGTMFG_SIZE];
	struct cgtmfgdata *data = (struct cgtmfgdata *)buf;

/* CONFIG_DM_SPI_FLASH is always active, even in SPL build process
 * so we have to use the IS_ENABLED switch to determine the right
 * condition */
#if CONFIG_IS_ENABLED(DM_SPI_FLASH)
        struct udevice *flash_dev = flash->dev;
	ret = spi_flash_read_dm(flash_dev, CGTMFG_ADDR_OFFSET, CGTMFG_SIZE, buf);
#else
	ret = spi_flash_read(flash, CGTMFG_ADDR_OFFSET, CGTMFG_SIZE, buf);
#endif
	if (ret) {
		printf("Error: SPI flash read error. Could not get the PN\n");
		return -ENODATA;
	}

	/* check if MFG data is valid and ensure that hwrev is set */
	md->is_valid = (_calc_mfg_chksum(data) == data->cksum);

	if (md->is_valid) {
		/* get hardware revision and memory size */
		md->hwrev_major = data->hwrev[0];
		md->hwrev_minor = data->hwrev[1];

		/* copy pn left aligned and ensure that it is a valid c-str */
		unsigned char pn_buf[sizeof(data->pn)+1];
		int len = _conv_pn_to_str(pn_buf, data->pn, sizeof(data->pn));
		if (len > (sizeof(md->pn)-1))
			len = (sizeof(md->pn)-1);
		memcpy(md->pn, pn_buf, len);
		md->pn[len] = 0;

		/* get memory size */
		md->mem_size = _get_memsize_from_pn(md->pn);
		debug("Module MFG data: PN%s / Rev.%c.%c / Mem %d\n",
			md->pn, md->hwrev_major, md->hwrev_minor, md->mem_size);
	}
	else {
		printf("Warning: Unable to read MFG data!\n");

		/* set default values */
		md->hwrev_major = '?';
		md->hwrev_minor = '?';

		memset(md->pn,0,sizeof(md->pn));
		md->mem_size = 1024;
	}

	return 0;
}
