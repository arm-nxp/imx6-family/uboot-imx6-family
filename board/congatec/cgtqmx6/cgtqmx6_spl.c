// SPDX-License-Identifier: GPL-2.0+
/*
 * SPL bootloader for congatec conga-QMX6
 *
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */


#include <asm/arch/clock.h>
#include <asm/arch/crm_regs.h>
#include <asm/arch/imx-regs.h>
#include <asm/arch/iomux.h>
#include <asm/arch/mx6-ddr.h>
#include <asm/arch/mx6-pins.h>
#include <asm/arch/sys_proto.h>
#include <asm/mach-imx/boot_mode.h>
#include <asm/mach-imx/mxc_i2c.h>
#include <hang.h>
#include <init.h>
#include <spi_flash.h>
#include <spi.h>
#include <spl.h>

#ifdef CONFIG_FSL_ESDHC_IMX
#include <fsl_esdhc_imx.h>
#endif

#include "qmx6_mfg.h"
#include "qmx6_spl_dram_init.h"

DECLARE_GLOBAL_DATA_PTR;

#define UART_PAD_CTRL  (PAD_CTL_PUS_100K_UP | PAD_CTL_SPEED_MED |\
	PAD_CTL_DSE_40ohm   | PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define USDHC_PAD_CTRL (PAD_CTL_PUS_47K_UP  | PAD_CTL_SPEED_LOW |\
	PAD_CTL_DSE_80ohm   | PAD_CTL_SRE_FAST  | PAD_CTL_HYS)

#define SPI_PAD_CTRL (PAD_CTL_HYS |				\
	PAD_CTL_SPEED_MED |		\
	PAD_CTL_DSE_40ohm | PAD_CTL_SRE_FAST)


/* conga-QMX6 onboard u-SD slot */
static iomux_v3_cfg_t const usdhc2_pads[] = {
	IOMUX_PADS(PAD_SD2_CLK__SD2_CLK    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD2_CMD__SD2_CMD    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD2_DAT0__SD2_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD2_DAT1__SD2_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD2_DAT2__SD2_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD2_DAT3__SD2_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_GPIO_4__GPIO1_IO04  | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
};

#if defined(CONFIG_CGTQMX6_EMMC_BOOT)
/* conga-QMX6 onboard eMMC */
static iomux_v3_cfg_t const usdhc3_pads[] = {
	IOMUX_PADS(PAD_SD3_CLK__SD3_CLK    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_CMD__SD3_CMD    | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT0__SD3_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT1__SD3_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT2__SD3_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT3__SD3_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT4__SD3_DATA4 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT5__SD3_DATA5 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT6__SD3_DATA6 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_DAT7__SD3_DATA7 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD3_RST__SD3_RESET  | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
};
#endif

#if defined(CONFIG_SD_BOOT)
/* Q7 connector SD interface */
static iomux_v3_cfg_t const usdhc4_pads[] = {
	IOMUX_PADS(PAD_SD4_CLK__SD4_CLK   | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_CMD__SD4_CMD   | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT0__SD4_DATA0 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT1__SD4_DATA1 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT2__SD4_DATA2 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT3__SD4_DATA3 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT4__SD4_DATA4 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT5__SD4_DATA5 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT6__SD4_DATA6 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_SD4_DAT7__SD4_DATA7 | MUX_PAD_CTRL(USDHC_PAD_CTRL)),
	IOMUX_PADS(PAD_NANDF_D6__GPIO2_IO06    | MUX_PAD_CTRL(NO_PAD_CTRL)),
};
#endif

#ifdef CONFIG_FSL_ESDHC_IMX

/* boot from onboard uSD card slot is only supported as failsafe boot */
static struct fsl_esdhc_cfg usdhc_cfg[] = {
{
	.esdhc_base = USDHC2_BASE_ADDR,
	.max_bus_width = 4,
	.wp_enable = 0,
	.vs18_enable = 0,
	},
};


int board_mmc_getcd(struct mmc *mmc)
{
	struct fsl_esdhc_cfg *cfg = (struct fsl_esdhc_cfg *)mmc->priv;
	int ret = 0;

	switch (cfg->esdhc_base) {
	case USDHC2_BASE_ADDR:
		gpio_direction_input(IMX_GPIO_NR(1, 4));
		ret = !gpio_get_value(IMX_GPIO_NR(1, 4));
		break;
	#if defined (CONFIG_CGTQMX6_EMMC_BOOT)
	case USDHC3_BASE_ADDR:
		ret = 1;	/* eMMC is always present */
		break;
	#endif
	#if defined (CONFIG_SD_BOOT)
	case USDHC4_BASE_ADDR:
		gpio_direction_input(IMX_GPIO_NR(2, 6));
		ret = !gpio_get_value(IMX_GPIO_NR(2, 6));
		break;
	#endif
	default:
		printf("Bad USDHC interface\n");
	}

	return ret;
}


/* Init onboard u-SD card interface */
int board_mmc_init(struct bd_info *bis)
{
	/* usdhc2: QMX6 onboard u-sd slot
	   usdhc3: QMX6 eMMC
	   usdhc4: QMX6 Q7-SD-interface
	*/

	int status = 0;

	usdhc_cfg[0].sdhc_clk = mxc_get_clock(MXC_ESDHC2_CLK);
	SETUP_IOMUX_PADS(usdhc2_pads);
	status = fsl_esdhc_initialize(bis, &usdhc_cfg[0]);

	#if defined (CONFIG_CGTQMX6_EMMC_BOOT)
	if (status)
		return status;
	usdhc_cfg[1].sdhc_clk = mxc_get_clock(MXC_ESDHC3_CLK);
	SETUP_IOMUX_PADS(usdhc3_pads);
	status = fsl_esdhc_initialize(bis, &usdhc_cfg[1]);
	#endif

	#if defined (CONFIG_SD_BOOT)
	if (status)
		return status;
	usdhc_cfg[2].sdhc_clk = mxc_get_clock(MXC_ESDHC4_CLK);
	SETUP_IOMUX_PADS(usdhc4_pads);
	status = fsl_esdhc_initialize(bis, &usdhc_cfg[2]);
	#endif

	return status;
}

#endif /* CONFIG_FSL_ESDHC_IMX */


/* USB PHY */
static const unsigned phy_bases[] = {
	USB_PHY0_BASE_ADDR,
	USB_PHY1_BASE_ADDR,
};

#define USBPHY_TX               0x00000010
#define USBPHY_TX_MSK_EDGE      (0x7 << 26)
#define USBPHY_TX_MSK_TXCAL45DP (0xF << 16)
#define USBPHY_TX_MSK_TXCAL45DN (0xF <<  8)
#define USBPHY_TX_MSK_D_CAL     (0xF <<  0)
#define USBPHY_TX_MSK (USBPHY_TX_MSK_EDGE | USBPHY_TX_MSK_TXCAL45DP | USBPHY_TX_MSK_TXCAL45DN | USBPHY_TX_MSK_D_CAL)

static iomux_v3_cfg_t const usb_otg_pads[] = {
	IOMUX_PADS(PAD_EIM_D22__USB_OTG_PWR | MUX_PAD_CTRL(NO_PAD_CTRL)),
	IOMUX_PADS(PAD_GPIO_1__USB_OTG_ID | MUX_PAD_CTRL(NO_PAD_CTRL)),
};

/* Init USB otg interface for uuu tool */
int board_ehci_hcd_init(int port)
{
	switch (port) {
	case 0:
		SETUP_IOMUX_PADS(usb_otg_pads);
		/*
		 * set daisy chain for otg_pin_id on 6q.
		 * for 6dl, this bit is reserved
		 */
		imx_iomux_set_gpr_register(1, 13, 1, 1);
		break;
	default:
		printf("Invalid USB port: %d\n", port);
		return -EINVAL;
	}

	return 0;
}

/* Power up USB PHY */
int board_ehci_power(int port, int on)
{
	void __iomem *phy_reg;
	void __iomem *phy_tx;
	u32 val;

	phy_reg = (void __iomem *)phy_bases[port];
	phy_tx = (void __iomem *)(phy_reg + USBPHY_TX);
	val = readl(phy_tx);

	val &= ~USBPHY_TX_MSK;
	val |=  (0x4 << 26);   // TX_EDGECONTROL = 0x100
	val |=  (0x1 << 16);   // TXCAL45DP = 0x1
	val |=  (0x1 << 8);    // TXCAL45DN = 0x1
	                       // D_CAL = 0x0
	writel(val, phy_tx);

	switch (port) {
	case 0:
		break;
	default:
		printf("Invalid USB port: %d\n", port);
		return -EINVAL;
	}

	return 0;
}


static iomux_v3_cfg_t const uart2_pads[] = {
	IOMUX_PADS(PAD_EIM_D26__UART2_TX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL)),
	IOMUX_PADS(PAD_EIM_D27__UART2_RX_DATA | MUX_PAD_CTRL(UART_PAD_CTRL)),
};

static void setup_iomux_uart(void)
{
	SETUP_IOMUX_PADS(uart2_pads);
}


#ifdef CONFIG_MXC_SPI

static iomux_v3_cfg_t const ecspi1_pads[] = {
	IOMUX_PADS(PAD_EIM_D16__ECSPI1_SCLK | MUX_PAD_CTRL(SPI_PAD_CTRL)),
	IOMUX_PADS(PAD_EIM_D17__ECSPI1_MISO | MUX_PAD_CTRL(SPI_PAD_CTRL)),
	IOMUX_PADS(PAD_EIM_D18__ECSPI1_MOSI | MUX_PAD_CTRL(SPI_PAD_CTRL)),
	IOMUX_PADS(PAD_EIM_D19__GPIO3_IO19 | MUX_PAD_CTRL(NO_PAD_CTRL)),
};

static void setup_iomux_spi(void)
{
	SETUP_IOMUX_PADS(ecspi1_pads);
	/* cs */
	gpio_direction_output(IMX_GPIO_NR(3, 19), 0);
}

int board_spi_cs_gpio(unsigned bus, unsigned cs)
{
	return (bus == 0 && cs == 0) ? (IMX_GPIO_NR(3, 19)) : -EINVAL;
}
#endif


int board_early_init_f(void)
{
	setup_iomux_uart();

#ifdef CONFIG_MXC_SPI
	setup_iomux_spi();
#else
#error  SPI interface needed for manufacturing data
#endif

	return 0;
}


static void ccgr_init(void)
{
	struct mxc_ccm_reg *ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;

	writel(0x00C03F3F, &ccm->CCGR0);
	writel(0x0030FC03, &ccm->CCGR1);
	writel(0x0FFFC000, &ccm->CCGR2);
	writel(0x3FF00000, &ccm->CCGR3);
	writel(0x00FFF300, &ccm->CCGR4);
	writel(0x0F0000C3, &ccm->CCGR5);
	writel(0x000003FF, &ccm->CCGR6);
}

/*
 * Board specific reset that does system reset.
 */
void reset_cpu(ulong addr)
{
}


/* initialize spi interface and spi flash (DM interface) */
static struct spi_flash* setup_spi_flash(void)
{
	struct spi_flash *flash;

	flash = spi_flash_probe(CONFIG_ENV_SPI_BUS,
			      CONFIG_ENV_SPI_CS,
			      CONFIG_ENV_SPI_MAX_HZ, CONFIG_ENV_SPI_MODE);

	if (!flash)
	{
		printf("Error: SPI flash initialization\n");
		hang();
	}


	return flash;
}


/* SPL entry point: init cpu, peripherals and console */
void board_init_f(ulong dummy)
{
	/* setup AIPS and disable watchdog */
	arch_cpu_init();

	ccgr_init();
	gpr_init();

	/* iomux */
	board_early_init_f();

	/* setup GP timer */
	timer_init();

	/* UART clocks enabled and gd valid - init serial console */
	preloader_console_init();

	/* needed for malloc() to work in SPL prior to board_init_r() */
	spl_init();

	/* spi flash for mfg data*/
	struct spi_flash *flash = setup_spi_flash();

	/* manufacturing data */
	struct cgtqmx6_mfg_data mfg_data;
	if (qmx6_mfg_read_data_from_flash(flash, &mfg_data))
		hang();

	/* print detected module */
	if (mfg_data.is_valid)
	{
		/* line will be continued in dram_init function */
		printf("conga-QMX6: PN:%s / Rev:%c.%c / RAM:%dMiB",
			mfg_data.pn,
			mfg_data.hwrev_major, mfg_data.hwrev_minor,
			mfg_data.mem_size);

		/* The SPL bootloader is only for standard congatec
		   conga-QMX6 modules. So ensure that this SPL bootloader
		   is not used with a CSA module accidentally. The CSA module
		   could be damaged or show undefined behaviour!
		 */
		if (mfg_data.pn[0] != '0') {
			printf("\n\nError: CSA detected! "
			  "This bootloader is only for standard conga-QMX6 modules!\n"
			  "Please use the non-SPL bootloader for CSA "
			  "with PN:%s\n",mfg_data.pn);
			hang();
		}
	}
	else
	{
		/* line will be continued in dram_init function */
		printf("conga-QMX6 using defaults: RAM:%dMiB", mfg_data.mem_size);
	}

	/* DDR initialization */
	qmx6_spl_dram_init(mfg_data.mem_size);

	/* clear the BSS. */
	memset(__bss_start, 0, __bss_end - __bss_start);

	/* load image from boot device and boot */
	/* does not return  */
	board_init_r(NULL, 0);
}


#ifdef CONFIG_MULTI_DTB_FIT
/* Identify DeviceTree name for current board */
int board_fit_config_name_match(const char *name)
{
	static const char * qmx6q = "imx6q-cgtqmx6";
	static const char * qmx6dl = "imx6dl-cgtqmx6";
	static const char * qmx6q_revE = "imx6q-cgtqmx6-revE";
	static const char * qmx6dl_revE = "imx6dl-cgtqmx6-revE";
	const char * model = NULL;

	/* read the mfg data again, because we are after
	 * relocation and file global data from early_init_f
	 * is not available any longer */
	struct spi_flash *flash = setup_spi_flash();
	struct cgtqmx6_mfg_data mfg_data;
	if (qmx6_mfg_read_data_from_flash(flash, &mfg_data))
		hang();

	if (!mfg_data.is_valid) {
		if (is_mx6dq())
			model = qmx6q_revE;
		else if (is_mx6sdl())
			model = qmx6dl_revE;
	}
	else {
		/* CSA revisions are different from standard modules */
		if (mfg_data.pn[0] != '0') {
			printf("CSA detected - this bootloader is only for standard conga-QMX6 modules\n");
			hang();
		}

		/* check for hardware revision of standard module */
		if (mfg_data.hwrev_major >= 'D') {
			if (is_mx6dq())
				model = qmx6q_revE;
			else if (is_mx6sdl())
				model = qmx6dl_revE;
		}
		else {
			if (is_mx6dq())
				model = qmx6q;
			else if (is_mx6sdl())
				model = qmx6dl;
		}
	}

	/* check if found */
	if (model) {
		if (!strcmp(name, model)) {
			return 0;
		}
	}

	return -1;
}
#endif
