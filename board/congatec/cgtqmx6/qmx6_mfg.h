/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Copyright (C) 2023-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#ifndef __QMX6_MFG_H
#define __QMX6_MFG_H

#include <spi_flash.h>

/* minimal struct for manufacturing data needed to initialize the module */
struct cgtqmx6_mfg_data {
	unsigned char pn[16];       /* part number */
	unsigned char hwrev_major;  /* hardware revision - major, eg 'X','A',... */
	unsigned char hwrev_minor;  /* hardware revision - minor, eg '0','1',... */
	int mem_size;               /* size of on module memory */
	bool is_valid;              /* flag, if MFG data on flash is valid or nort */
};


/* Read module manufacturing data from onboard SPI flash.
 * Returns -ENODATA if an error on SPI flash read occures, 0 otherwise
 * Also check md->valid to ensure data on flash is valid or not, eg
 * CRC error occured or MFG data is not written
 */
int qmx6_mfg_read_data_from_flash(struct spi_flash * flash, struct cgtqmx6_mfg_data * md);


#endif // __QMX6_MFG_H