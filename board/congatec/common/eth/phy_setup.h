/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Common setup of different ethernet PHYs
 *
 * Essential/basic configuration only, ie.
 * valid for all congatec ARM platforms
 *
 * Copyright (C) 2021-2024 congatec GmbH
 * Author: Georg Hartinger <georg.hartinger@congatec.com>
 */

#ifndef __CGT_PHY_SETUP_H
#define __CGT_PHY_SETUP_H

#include <phy.h>

int cgt_eth_phy_setup_ar8035(struct phy_device *phydev);
int cgt_eth_phy_setup_ar8031(struct phy_device *phydev);
int cgt_eth_phy_setup_dp83867(struct phy_device *phydev);

#endif // __CGT_PHY_SETUP_H
