// SPDX-License-Identifier: GPL-2.0+
/*
 * Common setup of different ethernet PHYs
 *
 * Essential/basic configuration only, ie.
 * valid for all congatec ARM platforms
 *
 * Copyright (C) 2021-2024 congatec GmbH
 *
 * Authors: Lukas Posadka <lukas.posadka@congatec.com>
 *          Georg Hartinger <georg.hartinger@congatec.com>
 *          Alex. Pockes <alexander.pockes@congatec.com>
 */

#include "phy_setup.h"
#include "ti_dp83867.h"
#include <micrel.h>


#ifdef CONFIG_PHY_ATHEROS

/* Essential common configuration of the Atheros AR8031 eth PHY */
int cgt_eth_phy_setup_ar8031(struct phy_device *phydev)
{
	u16 val = 0;

	ofnode node = phy_get_ofnode(phydev);
	if (!ofnode_valid(node)) {
		debug("DEBUG: can't get valid phy node\n");
		return -1;
	}
	debug("DEBUG: eth PHY: AR8031: ");

	bool sw1 = ofnode_read_bool(node, "cgt,ar8031,vddio_1V8");
	bool sw2 = ofnode_read_bool(node, "cgt,ar8031,rx_clk_delay");
	bool sw3 = ofnode_read_bool(node, "cgt,ar8031,tx_clk_delay");

	debug(" %i, %i, %i\n", sw1, sw2, sw3);

	/*
	  register type: debug registers
	   - select register by writing register offset to 0x1d
	   - read/write value from/to 0x1e
	*/

	if (sw1) { /* cgt,ar8031,vddio_1V8 */
		/* offset 0x1f -> PHY control debug register 0 */
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x1f);
		val = phy_read(phydev, MDIO_DEVAD_NONE, 0x1e);
		/* set bit 3 => SEL_1P5_1P8_POS_REG => 1V8 */
		val |= (1 << 3);
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, val);
		debug("DEBUG: writing 0x%04x to PCDR0\n", val);
	}

	if (sw2) { /* cgt,ar8031,rx_clk_del */
		/* offset 0x00 -> analog test control register */
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x00);
		val = phy_read(phydev, MDIO_DEVAD_NONE, 0x1e);
		/* set bit 15 => RGMII rx clock delay enable (default: 1/retain) */
		val |= (1 << 15);
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, val);
		debug("DEBUG: writing 0x%04x to ATCR\n", val);
	}

	if (sw3) { /* cgt,ar8031,tx_clk_del */
		/* offset 0x05 -> serdes test and system mode control register */
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x05);
		val = phy_read(phydev, MDIO_DEVAD_NONE, 0x1e);
		/* set bit 8 => RGMII tx clock delay enable (default: 0/0) */
		val |= (1 << 8);
		phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, val);
		debug("DEBUG: writing 0x%04x to SDTACR\n", val);
	}

	return 0;
}

/* Essential common configuration of the Atheros AR8035 eth PHY */
int cgt_eth_phy_setup_ar8035(struct phy_device *phydev)
{
	int val;
	int ret;
	debug("PHY: AR8035 ");

	/* enable AR8035 output a 125MHz clk from CLK_25M */
	ret = phy_write(phydev, MDIO_DEVAD_NONE, MMD_ACCESS_CONTROL, 0x7);
	phy_write(phydev, MDIO_DEVAD_NONE, MMD_ACCESS_REG_DATA,
		  MII_KSZ9031_MOD_DATA_POST_INC_RW | 0x16);
	phy_write(phydev, MDIO_DEVAD_NONE, MMD_ACCESS_CONTROL,
		  MII_KSZ9031_MOD_DATA_NO_POST_INC | 0x7);
	val = phy_read(phydev, MDIO_DEVAD_NONE, MMD_ACCESS_REG_DATA);
	val &= 0xfe63;
	val |= 0x18;
	phy_write(phydev, MDIO_DEVAD_NONE, MMD_ACCESS_REG_DATA, val);

	/* introduce tx clock delay */
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0x5);
	val = phy_read(phydev, MDIO_DEVAD_NONE, 0x1e);
	val |= 0x0100;
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, val);

	/* disable hibernation */
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1d, 0xb);
	val = phy_read(phydev, MDIO_DEVAD_NONE, 0x1e);
	ret = phy_write(phydev, MDIO_DEVAD_NONE, 0x1e, 0x3c40);

	// todo: suspend disable needed?

	// use result of last phy_write as indicator if something went wrong
	return ret;
}
#else // CONFIG_PHY_ATHEROS
int cgt_eth_phy_setup_ar8031(struct phy_device *phydev)
{
	return -ENODEV;
}
int cgt_eth_phy_setup_ar8035(struct phy_device *phydev)
{
	return -ENODEV;
}
#endif // CONFIG_PHY_ATHEROS


#ifdef CONFIG_PHY_TI
/* Essential common configuration of the TI DP83867 eth PHY */
int cgt_eth_phy_setup_dp83867(struct phy_device *phydev)
{
	int ret;
	u32 prop_val;
	u16 reg_val = 0;

	ofnode node = phy_get_ofnode(phydev);
	if (!ofnode_valid(node)) {
		debug("DEBUG: can't get valid phy node\n");
		return -1;
	}

	/* overwrite autonegotiation bootstrap pin setting */
	debug("\nPHY DP83867: config Autonegotiation\n");

	/* invert strap pin setting for autonegotiation */
	if (ofnode_read_bool(node, "cgt,dp83867,autonegotiation,invert_strap_pin")) {
		debug("DP83867: cgt,dp83867,autonegotiation,invert_strap_pin\n");
		reg_val = phy_read(phydev, phydev->addr, DP83867_BMCR);
		reg_val ^= DP83867_BMCR_AUTO_NEGOTIATION_ENABLE;
		debug("  DP83867_BMCR: 0x%x\n", reg_val);
		phy_write(phydev, MDIO_DEVAD_NONE, DP83867_BMCR, reg_val);
	}

	debug("\nPHY DP83867: config LEDs\n");

	/* LED function setup */
	reg_val = phy_read(phydev, phydev->addr, DP83867_LEDCR1);

	/* LED0 function */
	ret = ofnode_read_u32(node, "cgt,dp83867,led_0_sel", &prop_val);
	if (ret >= 0) {
		debug("DP83867: cgt,dp83867,led_0_sel=%d\n", prop_val);
		reg_val &= ~DP83867_LEDCR1_LED_0_SEL_MASK;
		reg_val |= (prop_val << DP83867_LEDCR1_LED_0_SEL_SHIFT);
	}

	/* LED1 function */
	ret = ofnode_read_u32(node, "cgt,dp83867,led_1_sel", &prop_val);
	if (ret >= 0) {
		debug("DP83867: cgt,dp83867,led_1_sel=%d\n", prop_val);
		reg_val &= ~DP83867_LEDCR1_LED_1_SEL_MASK;
		reg_val |= (prop_val << DP83867_LEDCR1_LED_1_SEL_SHIFT);
	}

	/* LED2 function */
	ret = ofnode_read_u32(node, "cgt,dp83867,led_2_sel", &prop_val);
	if (ret >= 0) {
		debug("DP83867: cgt,dp83867,led_2_sel=%d\n", prop_val);
		reg_val &= ~DP83867_LEDCR1_LED_2_SEL_MASK;
		reg_val |= (prop_val << DP83867_LEDCR1_LED_2_SEL_SHIFT);
	}

	/* on DP83867 GPIO 1 has to be used as LED 3 (if avail) */
	ret = ofnode_read_u32(node, "cgt,dp83867,gpio1_sel", &prop_val);
	if (ret >= 0) {
		/* check if used as LED3 */
		if (prop_val == DP83867_GPIO_LED3) {
			/* check also, if LED 3 node avail */
			ret = ofnode_read_u32(node, "cgt,dp83867,led_3_sel", &prop_val);
			if (ret >= 0) {
				/* configure GPIO 1 as LED 3 */
				int mux;
				debug("DP83867: cgt,dp83867,led_3_sel=%d (GPIO1)\n", prop_val);

				mux = phy_read_mmd(phydev, DP83867_DEVADDR, DP83867_GPIO_MUX_CTRL);
				mux &= ~DP83867_GPIO_MUX_GPIO_1_MASK;
				mux |= (DP83867_GPIO_LED3 << DP83867_GPIO_MUX_GPIO_1_SHIFT);
				phy_write_mmd(phydev, DP83867_DEVADDR, DP83867_GPIO_MUX_CTRL, mux);

				/* LED 3 */
				reg_val &= ~(DP83867_LEDCR1_LED_GPIO_SEL_MASK);
				reg_val |= (prop_val << DP83867_LEDCR1_LED_GPIO_SEL_SHIFT);
			}
		}
	}

	/* write LED function setup to LEDCR1 */
	phy_write(phydev, phydev->addr, DP83867_LEDCR1, reg_val);


	/* LED polarity setup */
	reg_val = phy_read(phydev, phydev->addr, DP83867_LEDCR2);

	/* LED0 polarity */
	if (ofnode_read_bool(node, "cgt,dp83867,led_0_pol,invert")) {
		debug("DP83867: cgt,dp83867,led_0_pol,invert\n");
		reg_val ^= DP83867_LEDCR2_LED_0_POLARITY;
	}

	/* LED1 polarity */
	if (ofnode_read_bool(node, "cgt,dp83867,led_1_pol,invert")) {
		debug("DP83867: cgt,dp83867,led_1_pol,invert\n");
		reg_val ^= DP83867_LEDCR2_LED_1_POLARITY;
	}

	/* LED2 polarity */
	if (ofnode_read_bool(node, "cgt,dp83867,led_2_pol,invert")) {
		debug("DP83867: cgt,dp83867,led_2_pol,invert\n");
		reg_val ^= DP83867_LEDCR2_LED_2_POLARITY;
	}

	/* LED3 polarity */
	if (ofnode_read_bool(node, "cgt,dp83867,led_3_pol,invert")) {
		debug("DP83867: cgt,dp83867,led_3_pol,invert\n");
		reg_val ^= DP83867_LEDCR2_LED_GPIO_POLARITY;
	}

	/* write LED polarity setup to LEDCR2 */
	phy_write(phydev, phydev->addr, DP83867_LEDCR2, reg_val);


	/* WOL setup */

	/* RXFCFG - read */
	reg_val = phy_read_mmd(phydev, DP83867_DEVADDR, DP83867_RXFCFG);

	/* RXFCFG - modify */
	/* select WOL mode: level */
	if (ofnode_read_bool(node, "cgt,dp83867,wol,level-mode")) {
		debug("DP83867: cgt,dp83867,wol,level-mode\n");
		reg_val |= DP83867_RXFCFG_WOL_OUT_MODE;
	}

	/* enable wake on magic packet */
	if (ofnode_read_bool(node, "cgt,dp83867,wol,wake-on-magic")) {
		debug("DP83867: cgt,dp83867,wol,wake-on-magic\n");
		reg_val |= DP83867_RXFCFG_WAKE_ON_MAGIC;
	}

	/* RXFCFG - write */
	phy_write_mmd(phydev, DP83867_DEVADDR, DP83867_RXFCFG, reg_val);

	/* CFG3 - mux INT/PWDN pin: enable INT output / disable PWDN input */
	if (ofnode_read_bool(node, "cgt,dp83867,mux,intpwdn,int-out")) {
		debug("DP83867: cgt,dp83867,mux,intpwdn,int-out\n");
		reg_val = phy_read(phydev, phydev->addr, DP83867_CFG3);
		reg_val |= DP83867_CFG3_INT_OE;
		phy_write(phydev, phydev->addr, DP83867_CFG3, reg_val);
	}

	/* MICR - enable WOL interrupts */
	if (ofnode_read_bool(node, "cgt,dp83867,int-src,wol")) {
		debug("DP83867: cgt,dp83867,int-src,wol\n");
		reg_val = phy_read(phydev, phydev->addr, DP83867_MICR);
		reg_val |= DP83867_MICR_WOL_INT_EN;
		phy_write(phydev, phydev->addr, DP83867_MICR, reg_val);
	}


	return 0;
}

#else // CONFIG_PHY_TI
int cgt_eth_phy_setup_dp83867(struct phy_device *phydev)
{
	return -ENODEV;
}
#endif // CONFIG_PHY_TI
