/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * TI DP83867 Ethernet PHY definitons
 *
 * Copyright (C) 2021-2024 congatec GmbH

 * Authors: Lukas Posadka <lukas.posadka@congatec.com>
 *          Georg Hartinger <georg.hartinger@congatec.com>
 *          Alex. Pockes <alexander.pockes@congatec.com>
 */

#ifndef __CGT_TI_DP83867_H
#define __CGT_TI_DP83867_H


/* address used for phy_read_mmd() */
/* MMD: MDIO Manageable Devices */
#define DP83867_DEVADDR                         0x1f

/* missing register definitions in dp83867.c */
#define DP83867_BMCR                            0x000
#define DP83867_PHYCR                           0x010
#define DP83867_LEDCR1                          0x018
#define DP83867_LEDCR2                          0x019
#define DP83867_GPIO_MUX_CTRL                   0x172
#define DP83867_CFG3                            0x01E
#define DP83867_MICR                            0x012
#define DP83867_RXFCFG                          0x134

/* TI DP83867: BMCR fields */
#define DP83867_BMCR_AUTO_NEGOTIATION_ENABLE    BIT(12)

/* TI DP83867: LEDCR1 fields */
#define DP83867_LEDCR1_LED_0_SEL_MASK           GENMASK(3, 0)
#define DP83867_LEDCR1_LED_0_SEL_SHIFT          0
#define DP83867_LEDCR1_LED_1_SEL_MASK           GENMASK(7, 4)
#define DP83867_LEDCR1_LED_1_SEL_SHIFT          4
#define DP83867_LEDCR1_LED_2_SEL_MASK           GENMASK(11, 9)
#define DP83867_LEDCR1_LED_2_SEL_SHIFT          8
#define DP83867_LEDCR1_LED_GPIO_SEL_MASK        GENMASK(15, 12)
#define DP83867_LEDCR1_LED_GPIO_SEL_SHIFT       12

/* TI DP83867: LEDCR2 bits */
#define DP83867_LEDCR2_LED_GPIO_POLARITY        BIT(14)
#define DP83867_LEDCR2_LED_GPIO_DRV_VAL         BIT(13)
#define DP83867_LEDCR2_LED_GPIO_DRV_EN          BIT(12)
#define DP83867_LEDCR2_LED_2_POLARITY           BIT(10)
#define DP83867_LEDCR2_LED_2_DRV_VAL            BIT(9)
#define DP83867_LEDCR2_LED_2_DRV_EN             BIT(8)
#define DP83867_LEDCR2_LED_1_POLARITY           BIT(6)
#define DP83867_LEDCR2_LED_1_DRV_VAL            BIT(5)
#define DP83867_LEDCR2_LED_1_DRV_EN             BIT(4)
#define DP83867_LEDCR2_LED_0_POLARITY           BIT(2)
#define DP83867_LEDCR2_LED_0_DRV_VAL            BIT(1)
#define DP83867_LEDCR2_LED_0_DRV_EN             BIT(0)

/* TI DP83867: LED functions definition */
#define DP83867_LED_RX_ERROR                    0xE
#define DP83867_LED_RX_OR_TX_ERROR              0xD
#define DP83867_LED_LINK_EST_TX_OR_RX_ACT       0xB
#define DP83867_LED_FULL_DUPLEX                 0xA
#define DP83867_LED_100_1000BT_LINK_EST         0x9
#define DP83867_LED_10_100BT_LINK_EST           0x8
#define DP83867_LED_10BT_LINK_EST               0x7
#define DP83867_LED_100_BTX_LINK_EST            0x6
#define DP83867_LED_1000BT_LINK_EST             0x5
#define DP83867_LED_COLLISION                   0x4
#define DP83867_LED_RX_ACT                      0x3
#define DP83867_LED_TX_ACT                      0x2
#define DP83867_LED_RX_OR_TX_ACT                0x1
#define DP83867_LED_LINK_EST                    0x0

/* TI DP83867: GPIO MUX CTRL fields */
#define DP83867_GPIO_MUX_GPIO_0_MASK            GENMASK(3, 0)
#define DP83867_GPIO_MUX_GPIO_0_SHIFT           0
#define DP83867_GPIO_MUX_GPIO_1_MASK            GENMASK(7, 4)
#define DP83867_GPIO_MUX_GPIO_1_SHIFT           4

/* TI DP83867: GPIO functions definitons */
#define DP83867_GPIO_ALWAYS_1                   0x9
#define DP83867_GPIO_ALWAYS_0                   0x8
#define DP83867_GPIO_PRBS_ERR_SYNC_LOSS         0x7
#define DP83867_GPIO_LED3                       0x6
#define DP83867_GPIO_ENERGY_DETECT              0x4
#define DP83867_GPIO_WOL                        0x3
#define DP83867_GPIO_1588_RX_SFD                0x2
#define DP83867_GPIO_1588_TX_SFD                0x1
#define DP83867_GPIO_0_RX_ER                    0x0
#define DP83867_GPIO_1_COL                      0x0

/* TI DP83867: CFG3 relevant fields */
#define DP83867_CFG3_INT_OE                     BIT(7)

/* TI DP83867: MICR relevant fields */
#define DP83867_MICR_WOL_INT_EN                 BIT(3)

/* TI DP83867: RXFCFG relevant fields */
#define DP83867_RXFCFG_WOL_OUT_MODE             BIT(8)
#define DP83867_RXFCFG_WAKE_ON_MAGIC            BIT(0)

#endif // __CGT_TI_DP83867_H
